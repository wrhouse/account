import base64
import hashlib
import time
from io import BytesIO
from typing import Tuple, Optional

import aiobotocore
from PIL import Image, ImageOps

from account.settings import Settings


settings = Settings()


async def upload_to_s3(filename: str,
                       data: BytesIO,
                       content_type='text/plain') -> bool:
    session = aiobotocore.get_session()
    async with session.create_client(
            's3',
            region_name='us-west-2',
            aws_secret_access_key=settings.aws_secret_access_key,
            aws_access_key_id=settings.aws_access_key_id) as client:
        response = await client.put_object(ACL='public-read',
                                           Bucket=settings.aws_bucket,
                                           Key=filename,
                                           ContentType=content_type,
                                           Body=data.getvalue())
        return response['ResponseMetadata']['HTTPStatusCode'] == 200


async def remove_from_s3(filename: str) -> bool:
    session = aiobotocore.get_session()
    async with session.create_client(
            's3', region_name='us-west-2',
            aws_secret_access_key=settings.aws_secret_access_key,
            aws_access_key_id=settings.aws_access_key_id) as client:
        response = await client.delete_object(Bucket=settings.aws_bucket, Key=filename)
    return response.get('ResponseMetadata', {}).get('HTTPStatusCode', 400) == 200


async def create_and_upload_image(base64_image: str,
                                  size: Tuple[int, int] = None,
                                  directory: str = 'public',
                                  salt: Optional[int] = None,
                                  filename: Optional[str] = None) -> str:
    if salt is None:
        salt = int(time.time())

    image = Image.open(BytesIO(base64.b64decode(base64_image))).convert('RGB')
    image_bytes = BytesIO()
    image.save(image_bytes, format='JPEG')
    if filename is None:
        filename = f"{hashlib.md5(image_bytes.getvalue()).hexdigest()}.{salt}"
    full_img_path = f"{directory}/{filename}.jpeg"

    if size:
        thumbnail = ImageOps.fit(image, size, Image.ANTIALIAS).convert('RGB')
        thumbnail_bytes = BytesIO()
        thumbnail.save(thumbnail_bytes, format='JPEG')
        await upload_to_s3(full_img_path, thumbnail_bytes, content_type='image/jpeg')
    else:
        await upload_to_s3(full_img_path, image_bytes, content_type='image/jpeg')

    return full_img_path
