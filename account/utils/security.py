from datetime import datetime, timedelta

from jwcrypto import jwk, jwt
import ujson as json


header = {
    "alg": "A256KW",
    "enc": "A256CBC-HS512"
}


def generate_token(payload: dict,
                   secret_key: str) -> str:
    claims = json.dumps(payload)
    token = jwt.JWT(header=header, claims=claims)
    key = jwk.JWK(k=secret_key, kty='oct')
    token.make_encrypted_token(key)
    return token.serialize()


def generate_access_token(user_id: int,
                          secret_key: str) -> str:
    payload = {
        'user_id': user_id,
        'created_at': datetime.utcnow().isoformat()
    }
    return generate_token(payload, secret_key)


def generate_refresh_token(access_token: str,
                           secret_key: str) -> str:
    payload = {
        'access_token': access_token,
        'created_at': datetime.utcnow().isoformat()
    }
    return generate_token(payload, secret_key)


def decode_payload(token: str,
                   secret_key: str) -> dict:
    key = jwk.JWK(k=secret_key, kty='oct')
    decoded_token = jwt.JWT(key=key, jwt=token)
    payload = json.loads(decoded_token.claims)
    if 'created_at' in payload:
        payload['created_at'] = datetime.fromisoformat(payload['created_at'])
    return payload


def token_is_expired(payload: dict,
                     expire_in: int) -> bool:
    if 'created_at' not in payload:
        return False
    current_time = datetime.utcnow()
    return (payload['created_at'] + timedelta(seconds=expire_in)) < current_time
