from typing import Optional, List

from aiohttp.web import Response
from aiohttp.web_exceptions import HTTPException
import ujson as json


class APIResponse(Response):
    status = 200

    def __init__(self,
                 data: Optional[dict] = None,
                 content_type: str = "application/json"):
        response_data = {"success": True}
        if data:
            response_data['data'] = data
        super().__init__(text=json.dumps(response_data), content_type=content_type)


class APIException(HTTPException):
    status_code = 400

    def __init__(
        self,
        error_code: int,
        error_message: str,
        content_type: str = "application/json",
    ):
        response_data = {
            "success": False,
            "error": {"code": error_code, "message": error_message},
        }
        super().__init__(text=json.dumps(response_data), content_type=content_type)


class InternalServerError(APIException):
    def __init__(self):
        APIException.__init__(
            self, error_code=1000, error_message="Internal server error"
        )


class MissingParameter(APIException):
    def __init__(self, param_name: str):
        APIException.__init__(
            self, error_code=1001, error_message=f"Missing parameter: {param_name}"
        )


class InvalidParameter(APIException):
    def __init__(self, param_name: str):
        APIException.__init__(
            self, error_code=1002, error_message=f"Invalid parameter: {param_name}"
        )


class DuplicateError(APIException):
    def __init__(self, param_name: str):
        APIException.__init__(
            self, error_code=1003, error_message=f"Duplicate parameter: {param_name}"
        )


class ValidationParametersError(APIException):
    def __init__(self, messages: List[str]):
        APIException.__init__(
            self, error_code=1004, error_message='; '.join(messages)
        )


class AccessTokenExpired(APIException):
    def __init__(self):
        APIException.__init__(
            self, error_code=1005, error_message='Access token has been expired'
        )


class AccessTokenDeleted(APIException):
    def __init__(self):
        APIException.__init__(
            self, error_code=1006, error_message='Access token has been deleted'
        )


class RefreshTokenExpired(APIException):
    def __init__(self):
        APIException.__init__(
            self, error_code=1007, error_message='Refresh token has been expired'
        )
