import traceback

from aiohttp.web import middleware
from aiohttp_apiset.exceptions import ValidationError

from account.responses import (
    APIResponse,
    APIException,
    InternalServerError,
    ValidationParametersError
)


@middleware
async def rest_api_response_formatter_middleware(request, handler):
    response = await handler(request)
    if response is None or isinstance(response, dict):
        return APIResponse(data=response)
    return response


@middleware
async def exception_handler_middleware(request, handler):
    try:
        response = await handler(request)
        return response
    except APIException as api_exception:
        raise api_exception from None
    except ValidationError as validation_error:
        messages = [message for messages_list in validation_error.to_flat().values() for message in messages_list]
        raise ValidationParametersError(messages) from None
    except Exception as msg:
        traceback.print_exc()
        raise InternalServerError() from None


SERVICE_MIDDLEWARES = [rest_api_response_formatter_middleware,
                       exception_handler_middleware]
