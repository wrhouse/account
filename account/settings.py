import os
from typing import Optional

from pydantic import BaseSettings


class Settings(BaseSettings):
    @property
    def service_host(self) -> str:
        return os.environ["SERVICE_HOST"]

    @property
    def service_port(self) -> int:
        return int(os.environ["SERVICE_PORT"])

    @property
    def environment(self) -> str:
        return os.environ["ENVIRONMENT"]

    @property
    def database_host(self) -> str:
        return os.environ["DATABASE_HOST"]

    @property
    def database_port(self) -> int:
        return int(os.environ["DATABASE_PORT"])

    @property
    def database_user(self) -> str:
        return os.environ["DATABASE_USER"]

    @property
    def database_password(self) -> str:
        return os.environ["DATABASE_PASSWORD"]

    @property
    def database_name(self) -> str:
        return os.environ["DATABASE_NAME"]

    @property
    def redis_host(self) -> str:
        return os.environ["REDIS_HOST"]

    @property
    def redis_port(self) -> int:
        return int(os.environ["REDIS_PORT"])

    @property
    def redis_database(self) -> int:
        return int(os.environ["REDIS_DATABASE"])

    @property
    def redis_password(self) -> Optional[str]:
        return os.environ.get("REDIS_PASSWORD") or None

    @property
    def aws_access_key_id(self) -> str:
        return os.environ["AWS_ACCESS_KEY_ID"]

    @property
    def aws_bucket(self) -> str:
        return os.environ["AWS_BUCKET"]

    @property
    def aws_secret_access_key(self) -> str:
        return os.environ["AWS_SECRET_ACCESS_KEY"]

    @property
    def user_image_large_size(self) -> (int, int):
        size = os.environ["USER_IMAGE_LARGE_SIZE"]
        return tuple(map(int, size.split('*')))

    @property
    def user_image_large_directory(self) -> str:
        return os.environ["USER_IMAGE_LARGE_DIRECTORY"]

    @property
    def user_image_small_size(self) -> (int, int):
        size = os.environ["USER_IMAGE_SMALL_SIZE"]
        return tuple(map(int, size.split('*')))

    @property
    def user_image_small_directory(self) -> str:
        return os.environ["USER_IMAGE_SMALL_DIRECTORY"]

    @property
    def token_expiration_check_delay(self) -> int:
        return int(os.environ["TOKEN_EXPIRATION_CHECK_DELAY"])

    @property
    def access_token_expire_in(self) -> int:
        return int(os.environ["ACCESS_TOKEN_EXPIRE_IN"])

    @property
    def access_token_secret_key(self) -> str:
        return os.environ["ACCESS_TOKEN_SECRET_KEY"]

    @property
    def refresh_token_expire_in(self) -> int:
        return int(os.environ["REFRESH_TOKEN_EXPIRE_IN"])

    @property
    def refresh_token_secret_key(self) -> str:
        return os.environ["REFRESH_TOKEN_SECRET_KEY"]
