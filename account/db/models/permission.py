from sqlalchemy import Column, String, Text

from account.migrations import Base


class Permission(Base):
    __tablename__ = 'permission'

    permission_id = Column(String(length=16), primary_key=True)
    description = Column(Text(), nullable=True)
