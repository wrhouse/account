from sqlalchemy import Column, String, Text, DateTime
from sqlalchemy.sql import func

from account.migrations import Base


class Group(Base):
    __tablename__ = 'group'

    group_id = Column(String(length=32), primary_key=True)

    name = Column(String(length=32), nullable=False, unique=True)
    description = Column(Text(), nullable=True)

    created_at = Column(DateTime(timezone=True), nullable=False, server_default=func.now())
