from sqlalchemy import Column, ForeignKey, Integer, String

from account.migrations import Base
from .account import Account
from .permission import Permission


class AccountPermissions(Base):
    __tablename__ = 'account_permissions'

    user_id = Column(Integer(), ForeignKey(Account.user_id, ondelete='CASCADE'), nullable=False, primary_key=True)
    permission_id = Column(String(length=16), ForeignKey(Permission.permission_id, ondelete='CASCADE'), nullable=False, primary_key=True)
