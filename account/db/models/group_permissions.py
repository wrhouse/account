from sqlalchemy import Column, ForeignKey, String

from account.migrations import Base
from .group import Group
from .permission import Permission


class GroupPermissions(Base):
    __tablename__ = 'group_permissions'

    group_id = Column(String(length=32), ForeignKey(Group.group_id, ondelete='CASCADE'), nullable=False, primary_key=True)
    permission_id = Column(String(length=16), ForeignKey(Permission.permission_id, ondelete='CASCADE'), nullable=False, primary_key=True)
