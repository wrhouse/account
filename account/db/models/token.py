from sqlalchemy import Column, DateTime, Text, String, Boolean, ForeignKey, Integer
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.sql import func, expression

from account.migrations import Base
from .account import Account


class Token(Base):
    __tablename__ = 'token'

    token_id = Column(UUID(as_uuid=True), primary_key=True)
    access_token = Column(Text(), nullable=False, unique=True)
    user_id = Column(Integer(), ForeignKey(Account.user_id, ondelete='CASCADE'), nullable=False)

    ip_address = Column(String(length=15), nullable=False)
    user_agent = Column(Text(), nullable=False)

    created_at = Column(DateTime(timezone=True), nullable=False, server_default=func.now())
    updated_at = Column(DateTime(timezone=True), nullable=False, server_default=func.now())

    is_expired = Column(Boolean(), nullable=False, server_default=expression.false())
    is_deleted = Column(Boolean(), nullable=False, server_default=expression.false())
