from sqlalchemy import Column, ForeignKey, Integer, String

from account.migrations import Base
from .account import Account
from .group import Group


class AccountGroups(Base):
    __tablename__ = 'account_groups'

    user_id = Column(Integer(), ForeignKey(Account.user_id, ondelete='CASCADE'), nullable=False, primary_key=True)
    group_id = Column(String(length=32), ForeignKey(Group.group_id, ondelete='CASCADE'), nullable=False, primary_key=True)
