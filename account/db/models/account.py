from sqlalchemy import Boolean, Column, Date, Enum, Integer, JSON, String, DateTime
from sqlalchemy.sql import expression, func

from account.db.enums import GenderTypes
from account.migrations import Base


class Account(Base):
    __tablename__ = 'account'

    user_id = Column(Integer(), primary_key=True)
    shop_user_id = Column(Integer(), nullable=True, index=True)
    forum_user_id = Column(Integer(), nullable=True, index=True)

    nickname = Column(String(length=32), nullable=False)
    nickname_lower = Column(String(length=32), nullable=False, unique=True)
    name = Column(String(length=64), nullable=True)
    surname = Column(String(length=64), nullable=True)

    email = Column(String(length=255), nullable=False, unique=True)
    email_is_verified = Column(Boolean(), nullable=False, server_default=expression.false())
    phone = Column(String(length=24), nullable=True, index=True)
    phone_is_verified = Column(Boolean(), nullable=False, server_default=expression.false())
    password = Column(String(length=64), nullable=False)

    vk_id = Column(String(length=32), nullable=False, server_default='', index=True)
    twitter_id = Column(String(length=32), nullable=False, server_default='', index=True)
    facebook_id = Column(String(length=32), nullable=False, server_default='', index=True)
    google_id = Column(String(length=32), nullable=False, server_default='', index=True)

    gender = Column(Enum(GenderTypes), nullable=False, server_default=GenderTypes.undefined.value)
    birth_date = Column(Date(), nullable=True)
    social = Column(JSON(), nullable=False, server_default='{}')

    image_large = Column(String(length=255), nullable=False, server_default='user_images/large/default.jpeg')
    image_small = Column(String(length=255), nullable=False, server_default='user_images/small/default.jpeg')

    created_at = Column(DateTime(timezone=True), nullable=False, server_default=func.now())
