from enum import Enum


class GenderTypes(Enum):
    male = "male"
    female = "female"
    undefined = "undefined"
