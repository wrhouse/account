from datetime import timedelta
import time

from aiopg.sa.result import RowProxy


UTC_OFFSET = timedelta(seconds=time.timezone)


def format_account(account: RowProxy) -> dict:
    result = dict(account)
    if 'gender' in result:
        result['gender'] = result['gender'].value
    if 'nickname_lower' in result:
        del result['nickname_lower']
    if 'birth_date' in result and result['birth_date'] is not None:
        result['birth_date'] = (result['birth_date'] + UTC_OFFSET).isoformat()
    if 'created_at' in result:
        result['created_at'] = (result['created_at'] + UTC_OFFSET).isoformat()
    return result


def format_group(group: RowProxy) -> dict:
    result = dict(group)
    if 'created_at' in result:
        result['created_at'] = (result['created_at'] + UTC_OFFSET).isoformat()
    return result


def format_permission(permission: RowProxy) -> dict:
    return dict(permission)


def format_token(token: RowProxy) -> dict:
    result = dict(token)
    if 'token_id' in result:
        result['token_id'] = str(result['token_id'])
    if 'created_at' in result:
        result['created_at'] = (result['created_at'] + UTC_OFFSET).isoformat()
    if 'updated_at' in result:
        result['updated_at'] = (result['updated_at'] + UTC_OFFSET).isoformat()
    return result
