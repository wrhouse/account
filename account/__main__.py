import asyncio

import aiohttp.web
import aiohttp_debugtoolbar
from aiohttp_apiset import SwaggerRouter
from aiopg.sa import create_engine
import aioredis
import uvloop

from account.daemons import expire_token
from account.middlewares import SERVICE_MIDDLEWARES
from account.settings import Settings
from account.routes import init_routes
from account.utils.enums import Environment


async def database_connection(app: aiohttp.web.Application):
    settings = app['settings']

    app['db'] = await create_engine(user=settings.database_user,
                                    password=settings.database_password,
                                    database=settings.database_name,
                                    host=settings.database_host,
                                    port=settings.database_port)

    expire_token_task = asyncio.create_task(expire_token(settings=settings,
                                                         engine=app['db']))

    yield

    expire_token_task.cancel()

    app['db'].close()
    await app['db'].wait_closed()


async def redis_connection(app: aiohttp.web.Application):
    settings = app['settings']

    redis_address = f"redis://{settings.redis_host}:{settings.redis_port}"
    app['redis'] = await aioredis.create_redis_pool(address=redis_address,
                                                    db=settings.redis_database,
                                                    password=settings.redis_password)

    yield

    app["redis"].close()
    await app["redis"].wait_closed()


def setup_server(settings: Settings) -> aiohttp.web.Application:
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

    swagger_ui = None
    if Environment(settings.environment) != Environment.PRODUCTION:
        swagger_ui = '/api/reference'
    app_router = SwaggerRouter(
        swagger_ui=swagger_ui,
        version_ui=2,
    )

    app = aiohttp.web.Application(router=app_router,
                                  middlewares=SERVICE_MIDDLEWARES)
    app['settings'] = settings

    init_routes(app=app)
    if Environment(settings.environment) != Environment.PRODUCTION:
        aiohttp_debugtoolbar.setup(app, intercept_redirects=False)

    app.cleanup_ctx.extend([database_connection,
                            redis_connection])

    return app


def create_app():
    settings = Settings()
    app = setup_server(settings)
    return app


if __name__ == '__main__':
    settings = Settings()

    app = setup_server(settings)
    aiohttp.web.run_app(app,
                        host=settings.service_host,
                        port=settings.service_port)
