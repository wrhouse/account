from sqlalchemy import select, literal, exists, and_

from account.db.models import AccountPermissions


async def is_exists(db_connection,
                    *,
                    user_id: int,
                    permission_id: str) -> bool:
    exists_query = (select([literal(True)])
                    .where(exists()
                           .where(and_(AccountPermissions.user_id == user_id,
                                       AccountPermissions.permission_id == permission_id))))
    cursor = await db_connection.scalar(exists_query)
    return bool(cursor)


async def create(db_connection,
                 *,
                 user_id: int,
                 permission_id: str) -> (int, str):
    insert_query = (AccountPermissions.__table__
                    .insert()
                    .values(user_id=user_id,
                            permission_id=permission_id))
    await db_connection.execute(insert_query)
    return user_id, permission_id


async def delete(db_connection,
                 *,
                 user_id: int,
                 permission_id: str) -> bool:
    delete_query = (AccountPermissions.__table__
                    .delete()
                    .where(and_(AccountPermissions.user_id == user_id,
                                AccountPermissions.permission_id == permission_id)))
    cursor = await db_connection.execute(delete_query)
    return bool(cursor.rowcount)
