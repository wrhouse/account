from typing import Optional, List

from sqlalchemy import select, literal, exists, and_

from account.db.models import AccountGroups


async def is_exists(db_connection,
                    *,
                    user_id: int,
                    group_id: str) -> bool:
    exists_query = (select([literal(True)])
                    .where(exists()
                           .where(and_(AccountGroups.user_id == user_id,
                                       AccountGroups.group_id == group_id))))
    cursor = await db_connection.scalar(exists_query)
    return bool(cursor)


async def get_list(db_connection,
                   *,
                   user_id: Optional[int] = None,
                   group_id: Optional[str] = None) -> List[AccountGroups]:
    select_query = AccountGroups.__table__.select()
    if user_id is not None:
        select_query = select_query.where(AccountGroups.user_id == user_id)
    if group_id is not None:
        select_query = select_query.where(AccountGroups.group_id == group_id)
    cursor = await db_connection.execute(select_query)
    return await cursor.fetchall()


async def create(db_connection,
                 *,
                 user_id: int,
                 group_id: str) -> (int, str):
    insert_query = (AccountGroups.__table__
                    .insert()
                    .values(user_id=user_id,
                            group_id=group_id))
    await db_connection.execute(insert_query)
    return user_id, group_id


async def delete(db_connection,
                 *,
                 user_id: int,
                 group_id: str) -> bool:
    delete_query = (AccountGroups.__table__
                    .delete()
                    .where(and_(AccountGroups.user_id == user_id,
                                AccountGroups.group_id == group_id)))
    cursor = await db_connection.execute(delete_query)
    return bool(cursor.rowcount)
