from typing import List, Optional

from sqlalchemy import select, literal, exists

from account.db.models import Permission


async def is_exists(db_connection,
                    *,
                    permission_id: str) -> bool:
    exists_query = (select([literal(True)])
                    .where(exists()
                           .where(Permission.permission_id == permission_id)))
    cursor = await db_connection.scalar(exists_query)
    return bool(cursor)


async def get_list(db_connection,
                   *,
                   fields: Optional[List[str]] = None) -> List[Permission]:
    if fields is None:
        select_query = Permission.__table__.select()
    else:
        select_query = select([Permission.__table__.c[field] for field in fields])

    cursor = await db_connection.execute(select_query)
    return await cursor.fetchall()


async def create(db_connection,
                 *,
                 permission_id: str,
                 description: Optional[str]) -> str:
    insert_query = (Permission.__table__
                    .insert()
                    .values(permission_id=permission_id,
                            description=description))
    await db_connection.execute(insert_query)
    return permission_id


async def update(db_connection,
                 *,
                 permission_id: str,
                 description: Optional[str]) -> str:
    update_query = (Permission.__table__
                    .update()
                    .where(Permission.permission_id == permission_id)
                    .values(description=description))
    await db_connection.execute(update_query)
    return permission_id


async def delete(db_connection,
                 *,
                 permission_id: str) -> bool:
    delete_query = (Permission.__table__
                    .delete()
                    .where(Permission.permission_id == permission_id))
    cursor = await db_connection.execute(delete_query)
    return bool(cursor.rowcount)
