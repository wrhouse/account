from typing import List, Optional

from sqlalchemy import select, literal, exists

from account.db.models import Group


async def is_exists(db_connection,
                    *,
                    group_id: str) -> bool:
    exists_query = (select([literal(True)])
                    .where(exists()
                           .where(Group.group_id == group_id)))
    cursor = await db_connection.scalar(exists_query)
    return bool(cursor)


async def get_list(db_connection,
                   *,
                   fields: Optional[List[str]] = None) -> List[Group]:
    if fields is None:
        select_query = Group.__table__.select()
    else:
        select_query = select([Group.__table__.c[field] for field in fields])
    cursor = await db_connection.execute(select_query)
    return await cursor.fetchall()


async def create(db_connection,
                 *,
                 group_id: str,
                 name: str,
                 description: Optional[str]) -> str:
    insert_query = (Group.__table__
                    .insert()
                    .values(group_id=group_id,
                            name=name,
                            description=description))
    await db_connection.execute(insert_query)
    return group_id


async def update(db_connection,
                 *,
                 group_id: str,
                 name: str,
                 description: Optional[str]) -> str:
    update_query = (Group.__table__
                    .update()
                    .where(Group.group_id == group_id)
                    .values(name=name,
                            description=description))
    await db_connection.execute(update_query)
    return group_id


async def delete(db_connection,
                 *,
                 group_id: str) -> bool:
    delete_query = (Group.__table__
                    .delete()
                    .where(Group.group_id == group_id))
    cursor = await db_connection.execute(delete_query)
    return bool(cursor.rowcount)

