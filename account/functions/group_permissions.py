from typing import Optional, List

from sqlalchemy import select, literal, exists, and_

from account.db.models import GroupPermissions


async def is_exists(db_connection,
                    *,
                    group_id: str,
                    permission_id: str) -> bool:
    exists_query = (select([literal(True)])
                    .where(exists()
                           .where(and_(GroupPermissions.group_id == group_id,
                                       GroupPermissions.permission_id == permission_id))))
    cursor = await db_connection.scalar(exists_query)
    return bool(cursor)


async def get_list(db_connection,
                   *,
                   group_id: Optional[str] = None,
                   permission_id: Optional[str] = None) -> List[GroupPermissions]:
    select_query = GroupPermissions.__table__.select()
    if group_id is not None:
        select_query = select_query.where(GroupPermissions.group_id == group_id)
    if permission_id is not None:
        select_query = select_query.where(GroupPermissions.permission_id == permission_id)
    cursor = await db_connection.execute(select_query)
    return await cursor.fetchall()


async def create(db_connection,
                 *,
                 group_id: str,
                 permission_id: str) -> (str, str):
    insert_query = (GroupPermissions.__table__
                    .insert()
                    .values(group_id=group_id,
                            permission_id=permission_id))
    await db_connection.execute(insert_query)
    return group_id, permission_id


async def delete(db_connection,
                 *,
                 group_id: str,
                 permission_id: str) -> bool:
    delete_query = (GroupPermissions.__table__
                    .delete()
                    .where(and_(GroupPermissions.group_id == group_id,
                                GroupPermissions.permission_id == permission_id)))
    cursor = await db_connection.execute(delete_query)
    return bool(cursor.rowcount)
