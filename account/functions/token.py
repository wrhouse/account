from datetime import datetime
from typing import Optional, List
import uuid

from sqlalchemy.sql import and_, select

from account.db.models import Token


async def get_list(db_connection,
                   *,
                   user_id: int,
                   is_deleted: Optional[bool] = None,
                   is_expired: Optional[bool] = None,
                   fields: Optional[List[str]] = None) -> List[Token]:
    if fields is None:
        select_query = Token.__table__.select()
    else:
        select_query = select([Token.__table__.c[field] for field in fields])

    select_query = select_query.where(Token.user_id == user_id)
    if is_deleted is not None:
        select_query = select_query.where(Token.is_deleted == is_deleted)
    if is_expired is not None:
        select_query = select_query.where(Token.is_expired == is_expired)

    cursor = await db_connection.execute(select_query)
    return await cursor.fetchall()


async def create_token(db_connection,
                       *,
                       access_token: str,
                       user_id: int,
                       ip_address: str,
                       user_agent: str) -> uuid.UUID:
    token_id = uuid.uuid4()
    insert_query = (Token.__table__
                    .insert()
                    .values(token_id=token_id,
                            access_token=access_token,
                            user_id=user_id,
                            ip_address=ip_address,
                            user_agent=user_agent))
    await db_connection.execute(insert_query)
    return token_id


async def refresh_access_token(db_connection,
                               *,
                               token_id: uuid.UUID,
                               access_token: str) -> None:
    update_query = (Token.__table__
                    .update()
                    .where(Token.token_id == token_id)
                    .values(access_token=access_token,
                            is_deleted=False,
                            is_expired=False,
                            updated_at=datetime.now()))
    await db_connection.execute(update_query)


async def get_token_by_id(db_connection,
                          *,
                          token_id: uuid.UUID) -> Optional[Token]:
    select_query = (Token.__table__
                    .select()
                    .where(and_(Token.token_id == token_id,
                                Token.is_deleted == False,
                                Token.is_expired == False)))
    cursor = await db_connection.execute(select_query)
    return await cursor.fetchone()


async def get_token_by_access_token(db_connection,
                                    *,
                                    access_token: str) -> Optional[Token]:
    select_query = (Token.__table__
                    .select()
                    .where(Token.access_token == access_token))
    cursor = await db_connection.execute(select_query)
    return await cursor.fetchone()


async def delete_tokens(db_connection,
                        *access_tokens) -> None:
    update_query = (Token.__table__
                    .update()
                    .where(Token.access_token.in_(access_tokens))
                    .values(is_deleted=False))
    await db_connection.execute(update_query)
