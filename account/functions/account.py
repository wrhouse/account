from datetime import date
from typing import Optional, Dict, List

from account.db.enums import GenderTypes
from sqlalchemy import select, literal, exists, join

from account.db.models import (
    Account,
    AccountGroups,
    AccountPermissions,
    GroupPermissions
)


async def is_exists(db_connection,
                    *,
                    user_id: Optional[int] = None,
                    nickname: Optional[str] = None,
                    email: Optional[str] = None,
                    vk_id: Optional[str] = None,
                    twitter_id: Optional[str] = None,
                    facebook_id: Optional[str] = None,
                    google_id: Optional[str] = None) -> bool:
    exists_subquery = exists()
    if user_id is not None:
        exists_subquery = exists_subquery.where(Account.user_id == user_id)
    if nickname is not None:
        exists_subquery = exists_subquery.where(Account.nickname_lower == nickname.lower())
    if email is not None:
        exists_subquery = exists_subquery.where(Account.email == email.lower())
    if vk_id is not None:
        exists_subquery = exists_subquery.where(Account.vk_id == vk_id)
    if twitter_id is not None:
        exists_subquery = exists_subquery.where(Account.twitter_id == twitter_id)
    if facebook_id is not None:
        exists_subquery = exists_subquery.where(Account.facebook_id == facebook_id)
    if google_id is not None:
        exists_subquery = exists_subquery.where(Account.google_id == google_id)

    exists_query = (select([literal(True)])
                    .where(exists_subquery))
    cursor = await db_connection.scalar(exists_query)
    return bool(cursor)


async def get_account_permission_ids(db_connection,
                                     *,
                                     user_id: int) -> List[str]:
    select_query = (select([AccountPermissions.permission_id])
                    .where(AccountPermissions.user_id == user_id))
    cursor = await db_connection.execute(select_query)
    permissions = await cursor.fetchall()
    return [permission.permission_id for permission in permissions]


async def get_account_group_permission_ids(db_connection,
                                           *,
                                           user_id: int) -> List[str]:
    inner_select_query = (select([AccountGroups.group_id])
                          .where(AccountGroups.user_id == user_id))
    select_query = (select([GroupPermissions.permission_id])
                    .where(GroupPermissions.group_id.in_(inner_select_query)))
    cursor = await db_connection.execute(select_query)
    permissions = await cursor.fetchall()
    return [permission.permission_id for permission in permissions]


async def get_account_groups(db_connection,
                             *,
                             user_id: int,
                             service_id: str) -> List[AccountGroups]:
    select_where = None
    if service_id == 'profile':
        select_where = Account.user_id == user_id
    elif service_id == 'shop':
        select_where = Account.shop_user_id == user_id
    elif service_id == 'forum':
        select_where = Account.forum_user_id == user_id

    if select_where is not None:
        select_query = (AccountGroups.__table__
                        .select()
                        .select_from(join(Account,
                                          AccountGroups,
                                          Account.user_id == AccountGroups.user_id))
                        .where(select_where))
        cursor = await db_connection.execute(select_query)
        return await cursor.fetchall()

    return []


async def get_account_by_id(db_connection,
                            *,
                            user_id: int,
                            service_id: str = 'profile',
                            fields: Optional[List[str]] = None) -> Optional[Account]:
    if fields is None:
        select_query = Account.__table__.select()
    else:
        select_query = select([Account.__table__.c[field] for field in fields])

    if service_id == 'profile':
        select_query = select_query.where(Account.user_id == user_id)
    elif service_id == 'shop':
        select_query = select_query.where(Account.shop_user_id == user_id)
    elif service_id == 'forum':
        select_query = select_query.where(Account.forum_user_id == user_id)

    cursor = await db_connection.execute(select_query)
    return await cursor.fetchone()


async def get_list(db_connection,
                   *,
                   count: int,
                   offset: int,
                   rev: int,
                   fields: Optional[List[str]] = None) -> List[Account]:
    if fields is None:
        select_query = Account.__table__.select()
    else:
        select_query = select([Account.__table__.c[field] for field in fields])

    # Конфигурирование порядка пользователей
    if rev == 0:
        select_query = select_query.order_by(Account.user_id.desc())
    elif rev == 1:
        select_query = select_query.order_by(Account.user_id.asc())

    # Ограничение количества пользователей в выдаче
    select_query = select_query.limit(count)

    # Смещение выдачи на указанный offset
    select_query = select_query.offset(offset)

    cursor = await db_connection.execute(select_query)
    return await cursor.fetchall()


async def search_users(db_connection,
                       *,
                       fields: Optional[List[str]] = None,
                       filters: Optional[dict] = None) -> List[Account]:
    if fields is None:
        select_query = Account.__table__.select()
    else:
        select_query = select([Account.__table__.c[field] for field in fields])

    if filters is not None:
        for field, value in filters.items():
            select_query = select_query.where(Account.__table__.c[field] == value)

    cursor = await db_connection.execute(select_query)
    return await cursor.fetchall()


async def create(db_connection,
                 *,
                 nickname: str,
                 name: Optional[str],
                 surname: Optional[str],
                 email: str,
                 password: str,
                 vk_id: Optional[str],
                 twitter_id: Optional[str],
                 facebook_id: Optional[str],
                 google_id: Optional[str],
                 gender: GenderTypes) -> int:
    insert_query = (Account.__table__
                    .insert()
                    .values(nickname=nickname,
                            nickname_lower=nickname.lower(),
                            name=name,
                            surname=surname,
                            email=email.lower(),
                            password=password,
                            vk_id=vk_id,
                            twitter_id=twitter_id,
                            facebook_id=facebook_id,
                            google_id=google_id,
                            gender=gender))
    return await db_connection.scalar(insert_query)


async def update(db_connection,
                 *,
                 user_id: int,
                 shop_user_id: Optional[int] = None,
                 forum_user_id: Optional[int] = None,
                 nickname: Optional[str] = None,
                 name: Optional[str] = None,
                 surname: Optional[str] = None,
                 email: Optional[str] = None,
                 email_is_verified: Optional[bool] = None,
                 phone: Optional[str] = None,
                 phone_is_verified: Optional[bool] = None,
                 password: Optional[str] = None,
                 vk_id: Optional[str] = None,
                 twitter_id: Optional[str] = None,
                 facebook_id: Optional[str] = None,
                 google_id: Optional[str] = None,
                 gender: Optional[GenderTypes] = None,
                 birth_date: Optional[date] = None,
                 social: Optional[Dict[str, str]] = None,
                 image_large: Optional[str] = None,
                 image_small: Optional[str] = None) -> int:
    update_data = dict()
    if shop_user_id is not None:
        update_data['shop_user_id'] = shop_user_id
    if forum_user_id is not None:
        update_data['forum_user_id'] = forum_user_id
    if nickname is not None:
        update_data['nickname'] = nickname
        update_data['nickname_lower'] = nickname.lower()
    if name is not None:
        update_data['name'] = name
    if surname is not None:
        update_data['surname'] = surname
    if email is not None:
        update_data['email'] = email.lower()
    if email_is_verified is not None:
        update_data['email_is_verified'] = email_is_verified
    if phone is not None:
        update_data['phone'] = phone
    if phone_is_verified is not None:
        update_data['phone_is_verified'] = phone_is_verified
    if password is not None:
        update_data['password'] = password
    if vk_id is not None:
        update_data['vk_id'] = vk_id
    if twitter_id is not None:
        update_data['twitter_id'] = twitter_id
    if facebook_id is not None:
        update_data['facebook_id'] = facebook_id
    if google_id is not None:
        update_data['google_id'] = google_id
    if gender is not None:
        update_data['gender'] = gender
    if birth_date is not None:
        update_data['birth_date'] = birth_date
    if social is not None:
        update_data['social'] = social
    if image_large is not None:
        update_data['image_large'] = image_large
    if image_small is not None:
        update_data['image_small'] = image_small

    update_query = (Account.__table__
                    .update()
                    .where(Account.user_id == user_id)
                    .values(**update_data))
    await db_connection.execute(update_query)

    return user_id
