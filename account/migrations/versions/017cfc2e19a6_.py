"""empty message

Revision ID: 017cfc2e19a6
Revises: 
Create Date: 2019-10-23 22:23:26.209543

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '017cfc2e19a6'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('account',
    sa.Column('user_id', sa.Integer(), nullable=False),
    sa.Column('shop_user_id', sa.Integer(), nullable=True),
    sa.Column('forum_user_id', sa.Integer(), nullable=True),
    sa.Column('nickname', sa.String(length=32), nullable=False),
    sa.Column('nickname_lower', sa.String(length=32), nullable=False),
    sa.Column('name', sa.String(length=64), nullable=True),
    sa.Column('surname', sa.String(length=64), nullable=True),
    sa.Column('email', sa.String(length=255), nullable=False),
    sa.Column('email_is_verified', sa.Boolean(), server_default=sa.text('false'), nullable=False),
    sa.Column('phone', sa.String(length=24), nullable=True),
    sa.Column('phone_is_verified', sa.Boolean(), server_default=sa.text('false'), nullable=False),
    sa.Column('password', sa.String(length=64), nullable=False),
    sa.Column('vk_id', sa.String(length=32), server_default='', nullable=False),
    sa.Column('twitter_id', sa.String(length=32), server_default='', nullable=False),
    sa.Column('facebook_id', sa.String(length=32), server_default='', nullable=False),
    sa.Column('google_id', sa.String(length=32), server_default='', nullable=False),
    sa.Column('gender', sa.Enum('male', 'female', 'undefined', name='gendertypes'), server_default='undefined', nullable=False),
    sa.Column('birth_date', sa.Date(), nullable=True),
    sa.Column('social', sa.JSON(), server_default='{}', nullable=False),
    sa.Column('image_large', sa.String(length=255), server_default='user_images/large/default.jpeg', nullable=False),
    sa.Column('image_small', sa.String(length=255), server_default='user_images/small/default.jpeg', nullable=False),
    sa.Column('created_at', sa.DateTime(timezone=True), server_default=sa.text('now()'), nullable=False),
    sa.PrimaryKeyConstraint('user_id'),
    sa.UniqueConstraint('email'),
    sa.UniqueConstraint('nickname_lower')
    )
    op.create_index(op.f('ix_account_facebook_id'), 'account', ['facebook_id'], unique=False)
    op.create_index(op.f('ix_account_forum_user_id'), 'account', ['forum_user_id'], unique=False)
    op.create_index(op.f('ix_account_google_id'), 'account', ['google_id'], unique=False)
    op.create_index(op.f('ix_account_phone'), 'account', ['phone'], unique=False)
    op.create_index(op.f('ix_account_shop_user_id'), 'account', ['shop_user_id'], unique=False)
    op.create_index(op.f('ix_account_twitter_id'), 'account', ['twitter_id'], unique=False)
    op.create_index(op.f('ix_account_vk_id'), 'account', ['vk_id'], unique=False)
    op.create_table('group',
    sa.Column('group_id', sa.String(length=32), nullable=False),
    sa.Column('name', sa.String(length=32), nullable=False),
    sa.Column('description', sa.Text(), nullable=True),
    sa.Column('created_at', sa.DateTime(timezone=True), server_default=sa.text('now()'), nullable=False),
    sa.PrimaryKeyConstraint('group_id'),
    sa.UniqueConstraint('name')
    )
    op.create_table('permission',
    sa.Column('permission_id', sa.String(length=16), nullable=False),
    sa.Column('description', sa.Text(), nullable=True),
    sa.PrimaryKeyConstraint('permission_id')
    )
    op.create_table('account_groups',
    sa.Column('user_id', sa.Integer(), nullable=False),
    sa.Column('group_id', sa.String(length=32), nullable=False),
    sa.ForeignKeyConstraint(['group_id'], ['group.group_id'], ondelete='CASCADE'),
    sa.ForeignKeyConstraint(['user_id'], ['account.user_id'], ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('user_id', 'group_id')
    )
    op.create_table('account_permissions',
    sa.Column('user_id', sa.Integer(), nullable=False),
    sa.Column('permission_id', sa.String(length=16), nullable=False),
    sa.ForeignKeyConstraint(['permission_id'], ['permission.permission_id'], ondelete='CASCADE'),
    sa.ForeignKeyConstraint(['user_id'], ['account.user_id'], ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('user_id', 'permission_id')
    )
    op.create_table('group_permissions',
    sa.Column('group_id', sa.String(length=32), nullable=False),
    sa.Column('permission_id', sa.String(length=16), nullable=False),
    sa.ForeignKeyConstraint(['group_id'], ['group.group_id'], ondelete='CASCADE'),
    sa.ForeignKeyConstraint(['permission_id'], ['permission.permission_id'], ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('group_id', 'permission_id')
    )
    op.create_table('token',
    sa.Column('token_id', postgresql.UUID(as_uuid=True), nullable=False),
    sa.Column('access_token', sa.Text(), nullable=False),
    sa.Column('user_id', sa.Integer(), nullable=False),
    sa.Column('ip_address', sa.String(length=15), nullable=False),
    sa.Column('user_agent', sa.Text(), nullable=False),
    sa.Column('created_at', sa.DateTime(timezone=True), server_default=sa.text('now()'), nullable=False),
    sa.Column('updated_at', sa.DateTime(timezone=True), server_default=sa.text('now()'), nullable=False),
    sa.Column('is_expired', sa.Boolean(), server_default=sa.text('false'), nullable=False),
    sa.Column('is_deleted', sa.Boolean(), server_default=sa.text('false'), nullable=False),
    sa.ForeignKeyConstraint(['user_id'], ['account.user_id'], ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('token_id'),
    sa.UniqueConstraint('access_token')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('token')
    op.drop_table('group_permissions')
    op.drop_table('account_permissions')
    op.drop_table('account_groups')
    op.drop_table('permission')
    op.drop_table('group')
    op.drop_index(op.f('ix_account_vk_id'), table_name='account')
    op.drop_index(op.f('ix_account_twitter_id'), table_name='account')
    op.drop_index(op.f('ix_account_shop_user_id'), table_name='account')
    op.drop_index(op.f('ix_account_phone'), table_name='account')
    op.drop_index(op.f('ix_account_google_id'), table_name='account')
    op.drop_index(op.f('ix_account_forum_user_id'), table_name='account')
    op.drop_index(op.f('ix_account_facebook_id'), table_name='account')
    op.drop_table('account')
    # ### end Alembic commands ###
