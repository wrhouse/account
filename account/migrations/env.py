from __future__ import with_statement

from logging.config import fileConfig

from alembic import context
from sqlalchemy import create_engine, pool

from account import migrations
from account.db.models import (
    Account,
    AccountGroups,
    AccountPermissions,
    Group,
    GroupPermissions,
    Permission,
    Token
)
from account.settings import Settings

print(Account)
print(AccountGroups)
print(AccountPermissions)
print(Group)
print(GroupPermissions)
print(Permission)
print(Token)

# import here you table files for autodetect your tables by the alembic

# this is the Alembic Config object, which provides
# access to the values within the .ini file in use.
config = context.config
settings = Settings()

config.set_section_option('user', 'sqlalchemy.url', settings.database_user)
config.set_section_option('password', 'sqlalchemy.url', settings.database_password)
config.set_section_option('host', 'sqlalchemy.url', settings.database_host)
config.set_section_option('port', 'sqlalchemy.url', str(settings.database_port))
config.set_section_option('db_name', 'sqlalchemy.url', settings.database_name)

# Interpret the config file for Python logging.
# This line sets up loggers basically.
fileConfig(config.config_file_name)

# add your model's MetaData object here
# for 'autogenerate' support
# from myapp import mymodel
# target_metadata = mymodel.Base.metadata
target_metadata = migrations.metadata


# other values from the config, defined by the needs of env.py,
# can be acquired:
# my_important_option = config.get_main_option("my_important_option")
# ... etc.


def run_migrations_offline() -> None:
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    """
    url = config.get_main_option("sqlalchemy.url")
    context.configure(url=url,
                      target_metadata=target_metadata,
                      literal_binds=True)

    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online() -> None:
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    settings = Settings()
    dsn = 'postgresql+psycopg2://{user}:{password}@{host}:{port}/{db_name}'.format(user=settings.database_user,
                                                                                   password=settings.database_password,
                                                                                   host=settings.database_host,
                                                                                   port=settings.database_port,
                                                                                   db_name=settings.database_name)
    connectable = create_engine(dsn, poolclass=pool.NullPool)

    with connectable.connect() as connection:
        context.configure(connection=connection,
                          target_metadata=target_metadata)

        with context.begin_transaction():
            context.run_migrations()


if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
