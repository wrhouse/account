import aiohttp.web

from account.db.json_formatters import format_token
import account.functions.token as token_func


async def get_user_tokens(request: aiohttp.web.Request):
    """
    ---
    summary: Получение списка токенов (сессий) пользователя
    tags:
        - Token
    produces:
        - application/json
    parameters:
        - name: user_id
          in: path
          type: integer
          required: true
          minimum: 1
          description: Идентификатор пользователя, сессии которого требуется вернуть.
        - name: is_deleted
          in: query
          type: integer
          required: false
          minimum: 0
          maximum: 1
          description: Флаг, означающий, что необходимо вернуть только удаленные (1)
                       или не удаленные вручную сессии (0).
        - name: is_expired
          in: query
          type: integer
          required: false
          minimum: 0
          maximum: 1
          description: Флаг, означающий, что необходимо вернуть только устаревшие (1)
                       или не устаревшие сессии (0).
        - name: fields
          in: query
          type: string
          required: false
          description: Список полей, перечисленных через запятую, которые требуется вернуть
    responses:
        200:
            description: OK
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: true
                    data:
                        type: object
                        properties:
                            tokens:
                                type: array
                                description: Список сессий (токенов) пользователя.
                                items:
                                    type: object
                                    properties:
                                        token_id:
                                            type: string
                                            format: uuid
                                            description: Уникальный идентификатор сессии пользователя.
                                        access_token:
                                            type: string
                                            description: Токен доступа пользователя, соответствующий сессии.
                                        user_id:
                                            type: integer
                                            description: Идентификатор пользователя, которому принадлежит сессия.
                                        ip_address:
                                            type: string
                                            format: ipv4
                                            description: IP адрес, с которого был авторизован пользователь.
                                        user_agent:
                                            type: string
                                            description: User-Agent браузера, с которого был авторизован пользователь.
                                        created_at:
                                            type: string
                                            description: Дата и время создания токена (сессии) в ISO формате.
                                        updated_at:
                                            type: string
                                            description: Дата и время последнего обновления токена в ISO формате.
                                        is_expired:
                                            type: boolean
                                            description: Флаг, означающий, что сессия устарела (true).
                                        is_deleted:
                                            type: boolean
                                            description: Флаг, означающий, что сессия была удалена вручную (true).
                        required:
                            - tokens
                required:
                    - success
                    - data
        400:
            description: Bad Request
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: false
                    error:
                        type: object
                        properties:
                            code:
                                type: integer
                                description: Код ошибки.
                                example: 1000
                            message:
                                type: string
                                description: Описание ошибки.
                                example: Internal server error
                        required:
                            - code
                            - message
                required:
                    - success
                    - error
    """
    user_id = int(request.match_info['user_id'])

    is_deleted = request.rel_url.query.get('is_deleted') or None
    if is_deleted is not None:
        is_deleted = bool(int(is_deleted))

    is_expired = request.rel_url.query.get('is_expired') or None
    if is_expired is not None:
        is_expired = bool(int(is_expired))

    fields = request.rel_url.query.get('fields') or None
    if fields is not None:
        fields = fields.split(',')

    async with request.app['db'].acquire() as connection:
        tokens = await token_func.get_list(connection,
                                           user_id=user_id,
                                           is_deleted=is_deleted,
                                           is_expired=is_expired,
                                           fields=fields)
    tokens = [format_token(token=token) for token in tokens]

    return {
        'tokens': tokens
    }
