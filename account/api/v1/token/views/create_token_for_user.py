import aiohttp.web

import account.functions.token as token_func
from account.utils import security


async def create_token_for_user(request: aiohttp.web.Request):
    """
    ---
    summary: Создание новой пары токенов доступа для пользователя
    tags:
        - Token
    produces:
        - application/json
    parameters:
        - name: user_id
          in: path
          type: integer
          required: true
          minimum: 1
          description: Основной идентификатор пользователя.
        - name: data
          in: body
          required: true
          schema:
            type: object
            properties:
                ip_address:
                    type: string
                    description: IP адрес, с которого авторизовался пользователь
                    format: ipv4
                user_agent:
                    type: string
                    description: User-Agent браузера, с которого авторизовался пользователь
            required:
                - ip_address
                - user_agent
    responses:
        200:
            description: OK
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: true
                    data:
                        type: object
                        properties:
                            access_token:
                                type: string
                                description: Токен доступа для текущей сессии пользователя.
                            refresh_token:
                                type: string
                                description: Токен, необходимый для обновления пары (access_token, refresh_token).
                        required:
                            - access_token
                            - refresh_token
                required:
                    - success
                    - data
        400:
            description: Bad Request
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: false
                    error:
                        type: object
                        properties:
                            code:
                                type: integer
                                description: Код ошибки.
                                example: 1000
                            message:
                                type: string
                                description: Описание ошибки.
                                example: Internal server error
                        required:
                            - code
                            - message
                required:
                    - success
                    - error
    """
    settings = request.app['settings']
    request_data = await request.json()

    user_id = int(request.match_info['user_id'])
    ip_address = request_data['ip_address']
    user_agent = request_data['user_agent']

    access_token = security.generate_access_token(user_id=user_id,
                                                  secret_key=settings.access_token_secret_key)
    refresh_token = security.generate_refresh_token(access_token=access_token,
                                                    secret_key=settings.refresh_token_secret_key)

    async with request.app['db'].acquire() as connection:
        token_id = await token_func.create_token(connection,
                                                 access_token=access_token,
                                                 user_id=user_id,
                                                 ip_address=ip_address,
                                                 user_agent=user_agent)

    await request.app['redis'].set(key=access_token,
                                   value=str(token_id),
                                   expire=settings.access_token_expire_in)

    return {
        'access_token': access_token,
        'refresh_token': refresh_token
    }
