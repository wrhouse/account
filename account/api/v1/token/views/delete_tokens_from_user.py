import uuid

import aiohttp.web

import account.functions.token as token_func


async def delete_tokens_from_user(request: aiohttp.web.Request):
    """
    ---
    summary: Завершение всех сессий пользователя
    description: За исключением тех, что указаны в whitelist.
    tags:
        - Token
    produces:
        - application/json
    parameters:
        - name: user_id
          in: path
          type: integer
          required: true
          minimum: 1
          description: Основной идентификатор пользователя, сессии которого необходимо завершить.
        - name: token_data
          in: body
          required: false
          schema:
            type: object
            properties:
                whitelist:
                    type: array
                    description: Список токенов доступа или идентификаторов сессий (UUID), которые не нужно завершать.
                    items:
                        type: string
    responses:
        200:
            description: OK
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: true
                required:
                    - success
        400:
            description: Bad Request
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: false
                    error:
                        type: object
                        properties:
                            code:
                                type: integer
                                description: Код ошибки.
                                example: 1000
                            message:
                                type: string
                                description: Описание ошибки.
                                example: Internal server error
                        required:
                            - code
                            - message
                required:
                    - success
                    - error
    """
    user_id = int(request.match_info['user_id'])
    token_data = await request.json()
    whitelist = token_data.get('whitelist') or []
    whitelist_access_tokens = set()
    whitelist_token_ids = set()

    for item in whitelist:
        try:
            token_id = uuid.UUID(item)
            whitelist_token_ids.add(token_id)
        except ValueError:
            whitelist_access_tokens.add(item)

    async with request.app['db'].acquire() as connection:
        user_tokens = await token_func.get_list(connection,
                                                user_id=user_id,
                                                is_active=True)

    access_tokens_to_delete = []
    for user_token in user_tokens:
        if user_token.token_id not in whitelist_token_ids and \
                user_token.access_token not in whitelist_access_tokens:
            access_tokens_to_delete.append(user_token.access_token)

    await request.app['redis'].delete(*access_tokens_to_delete)

    async with request.app['db'].acquire() as connection:
        await token_func.delete_tokens(connection, *access_tokens_to_delete)
