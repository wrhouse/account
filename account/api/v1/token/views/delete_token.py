import uuid

import aiohttp.web

import account.functions.token as token_func
from account.responses import MissingParameter, InvalidParameter


async def delete_token(request: aiohttp.web.Request):
    """
    ---
    summary: Завершение сессии пользователя по ее идентификатору или токену доступа
    tags:
        - Token
    produces:
        - application/json
    parameters:
        - name: token_data
          in: body
          required: true
          schema:
            type: object
            properties:
                token_id:
                    type: string
                    format: uuid
                    description: Идентификатор сессии, которую требуется удалить.
                access_token:
                    type: string
                    description: Токен доступа пользователя, сессию которого требуется удалить.
    responses:
        200:
            description: OK
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: true
                required:
                    - success
        400:
            description: Bad Request
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: false
                    error:
                        type: object
                        properties:
                            code:
                                type: integer
                                description: Код ошибки.
                                example: 1000
                            message:
                                type: string
                                description: Описание ошибки.
                                example: Internal server error
                        required:
                            - code
                            - message
                required:
                    - success
                    - error
    """
    token_data = await request.json()
    token_id = token_data.get('token_id') or None
    access_token = token_data.get('access_token') or None

    if access_token is None and token_id is None:
        raise MissingParameter(param_name='token_id or access_token')

    async with request.app['db'].acquire() as connection:
        if token_id is not None:
            token = await token_func.get_token_by_id(connection,
                                                     token_id=uuid.UUID(token_id))
            if token is None:
                raise InvalidParameter(param_name='token_id')
            access_token = token.access_token

    await request.app['redis'].delete(access_token)

    async with request.app['db'].acquire() as connection:
        await token_func.delete_tokens(connection, access_token)
