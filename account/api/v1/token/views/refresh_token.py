import aiohttp.web

import account.functions.token as token_func
from account.responses import RefreshTokenExpired, InvalidParameter
from account.utils import security


async def refresh_token(request: aiohttp.web.Request):
    """
    ---
    summary: Обновление сессии пользователя
    tags:
        - Token
    produces:
        - application/json
    parameters:
        - name: data
          in: body
          required: true
          schema:
            type: object
            properties:
                refresh_token:
                    type: string
                    description: Токен, используемый для обновления пары (access_token, refresh_token).
            required:
                - refresh_token
    responses:
        200:
            description: OK
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: true
                    data:
                        type: object
                        properties:
                            access_token:
                                type: string
                                description: Токен доступа для текущей сессии пользователя.
                            refresh_token:
                                type: string
                                description: Токен, необходимый для обновления пары (access_token, refresh_token).
                        required:
                            - access_token
                            - refresh_token
                required:
                    - success
                    - data
        400:
            description: Bad Request
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: false
                    error:
                        type: object
                        properties:
                            code:
                                type: integer
                                description: Код ошибки.
                                example: 1000
                            message:
                                type: string
                                description: Описание ошибки.
                                example: Internal server error
                        required:
                            - code
                            - message
                required:
                    - success
                    - error
    """
    settings = request.app['settings']
    request_data = await request.json()
    refresh_token = request_data['refresh_token']

    try:
        payload = security.decode_payload(token=refresh_token,
                                          secret_key=settings.refresh_token_secret_key)
        is_expired = security.token_is_expired(payload=payload,
                                               expire_in=settings.refresh_token_expire_in)
        if is_expired:
            raise RefreshTokenExpired()
    except ValueError:
        raise InvalidParameter(param_name='refresh_token')

    access_token = payload['access_token']
    await request.app['redis'].delete(access_token)

    async with request.app['db'].acquire() as connection:
        token = await token_func.get_token_by_access_token(connection,
                                                           access_token=access_token)

    if token.is_deleted:
        raise RefreshTokenExpired()

    access_token = security.generate_access_token(user_id=token.user_id,
                                                  secret_key=settings.access_token_secret_key)
    refresh_token = security.generate_refresh_token(access_token=access_token,
                                                    secret_key=settings.refresh_token_secret_key)

    async with request.app['db'].acquire() as connection:
        await token_func.refresh_access_token(connection,
                                              token_id=token.token_id,
                                              access_token=access_token)

    await request.app['redis'].set(key=access_token,
                                   value=str(token.token_id),
                                   expire=settings.access_token_expire_in)

    return {
        'access_token': access_token,
        'refresh_token': refresh_token
    }