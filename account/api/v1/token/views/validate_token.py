import aiohttp.web

from account.responses import InvalidParameter, AccessTokenDeleted, AccessTokenExpired
from account.utils import security


async def validate_token(request: aiohttp.web.Request):
    """
    ---
    summary: Валидация токена доступа
    tags:
        - Token
    produces:
        - application/json
    parameters:
        - name: access_token
          in: query
          type: string
          description: Токен доступа пользователя, валидность которого требуется проверить.
          required: true
    responses:
        200:
            description: OK
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: true
                required:
                    - success
        400:
            description: Bad Request
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: false
                    error:
                        type: object
                        properties:
                            code:
                                type: integer
                                description: Код ошибки.
                                example: 1000
                            message:
                                type: string
                                description: Описание ошибки.
                                example: Internal server error
                        required:
                            - code
                            - message
                required:
                    - success
                    - error
    """
    settings = request.app['settings']
    access_token = request.rel_url.query['access_token']

    try:
        payload = security.decode_payload(token=access_token,
                                          secret_key=settings.access_token_secret_key)
        is_expired = security.token_is_expired(payload=payload,
                                               expire_in=settings.access_token_expire_in)
        if is_expired:
            raise AccessTokenExpired()
    except ValueError:
        raise InvalidParameter(param_name='access_token')

    is_exists = await request.app['redis'].exists(access_token)
    if not is_exists:
        raise AccessTokenDeleted()
