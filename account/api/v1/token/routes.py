import aiohttp.web

from account.api.v1.token.views.create_token_for_user import create_token_for_user
from account.api.v1.token.views.delete_token import delete_token
from account.api.v1.token.views.delete_tokens_from_user import delete_tokens_from_user
from account.api.v1.token.views.get_user_tokens import get_user_tokens
from account.api.v1.token.views.refresh_token import refresh_token
from account.api.v1.token.views.validate_token import validate_token


def init_routes(app: aiohttp.web.Application, prefix):
    app.router.add_get(prefix, validate_token)
    app.router.add_put(prefix, refresh_token)
    app.router.add_delete(prefix, delete_token)
    app.router.add_get(prefix + r'/users/{user_id:\d+}', get_user_tokens)
    app.router.add_post(prefix + r'/users/{user_id:\d+}', create_token_for_user)
    app.router.add_delete(prefix + r'/users/{user_id:\d+}', delete_tokens_from_user)

