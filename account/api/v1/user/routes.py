import aiohttp.web

from account.api.v1.user.views.create_user import create_user
from account.api.v1.user.views.get_user_by_id import get_user_by_id
from account.api.v1.user.views.get_user_groups_by_id import get_user_groups_by_id
from account.api.v1.user.views.get_user_permissions_by_id import get_user_permissions_by_id
from account.api.v1.user.views.get_users_list import get_users_list
from account.api.v1.user.views.search_users import search_users
from account.api.v1.user.views.update_user import update_user
from account.api.v1.user.views.verify_email import verify_email
from account.api.v1.user.views.verify_phone import verify_phone


def init_routes(app: aiohttp.web.Application, prefix):
    app.router.add_get(prefix, get_users_list)
    app.router.add_post(prefix, create_user)
    app.router.add_get(prefix + '/search', search_users)
    app.router.add_get(prefix + r'/{user_id:\d+}', get_user_by_id)
    app.router.add_post(prefix + r'/{user_id:\d+}', update_user)
    app.router.add_get(prefix + r'/{user_id:\d+}/groups', get_user_groups_by_id)
    app.router.add_get(prefix + r'/{user_id:\d+}/permissions', get_user_permissions_by_id)
    app.router.add_post(prefix + r'/{user_id:\d+}/verify/email', verify_email)
    app.router.add_post(prefix + r'/{user_id:\d+}/verify/phone', verify_phone)
