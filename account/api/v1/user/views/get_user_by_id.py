import aiohttp.web

from account.db.json_formatters import format_account
import account.functions.account as account_func
from account.responses import InvalidParameter


async def get_user_by_id(request: aiohttp.web.Request):
    """
    ---
    summary: Получение информации о пользователе по его идентификатору
    tags:
        - User
    produces:
        - application/json
    parameters:
        - name: user_id
          in: path
          type: integer
          required: true
          minimum: 1
          description: Идентификатор пользователя, информацию о котором требуется вернуть.
        - name: service_id
          in: query
          type: string
          required: false
          default: profile
          description: Идентификатор сервиса, которому принадлежит user_id.
        - name: fields
          in: query
          type: string
          required: false
          description: Список полей, перечисленных через запятую, которые требуется вернуть
    responses:
        200:
            description: OK
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: true
                    data:
                        type: object
                        properties:
                            user_id:
                                type: integer
                                description: Основной идентификатор пользователя.
                            shop_user_id:
                                type: integer
                                description: Идентификатор пользователя в сервисе shop.
                                x-nullable: true
                            forum_user_id:
                                type: integer
                                description: Идентификатор пользователя в сервисе forum.
                                x-nullable: true
                            nickname:
                                type: string
                                description: Никнейм пользователя.
                            name:
                                type: string
                                description: Имя пользователя.
                                x-nullable: true
                            surname:
                                type: string
                                description: Фамилия пользователя.
                                x-nullable: true
                            email:
                                type: string
                                description: Адрес электронной почты пользователя.
                            email_is_verified:
                                type: boolean
                                description: Флаг, показывающий верифицирован ли email пользователя (true)
                                             или нет (false).
                            phone:
                                type: string
                                description: Номер телефона пользователя.
                                x-nullable: true
                            phone_is_verified:
                                type: boolean
                                description: Флаг, показывающий верифицирован ли номер телефона
                                             пользователя (true) или нет (false).
                            password:
                                type: string
                                description: Хэш пароля пользователя.
                            vk_id:
                                type: string
                                description: Идентификатор пользователя в социальной сети ВКонтакте.
                                x-nullable: true
                            twitter_id:
                                type: string
                                description: Идентификатор пользователя в социальной сети Twitter.
                                x-nullable: true
                            facebook_id:
                                type: string
                                description: Идентификатор пользователя в социальной сети Facebook.
                                x-nullable: true
                            google_id:
                                type: string
                                description: Идентификатор пользователя в сервисах Google.
                                x-nullable: true
                            gender:
                                type: string
                                description: Пол пользователя.
                            birth_date:
                                type: string
                                description: Дата рождения пользователя в ISO формате.
                            social:
                                type: object
                                description: Словарь с юзер неймами пользователя в мессенджерах.
                            image_large:
                                type: string
                                description: Ссылка (без хоста) на аватар пользователя (большой).
                                x-nullable: true
                            image_small:
                                type: string
                                description: Ссылка (без хоста) на аватар пользователя (маленький).
                                x-nullable: true
                            created_at:
                                type: string
                                description: Дата и время регистрации пользователя в ISO формате.
                required:
                    - success
                    - data
        400:
            description: Bad Request
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: false
                    error:
                        type: object
                        properties:
                            code:
                                type: integer
                                description: Код ошибки.
                                example: 1000
                            message:
                                type: string
                                description: Описание ошибки.
                                example: Internal server error
                        required:
                            - code
                            - message
                required:
                    - success
                    - error
    """
    user_id = int(request.match_info['user_id'])
    service_id = request.rel_url.query.get('service_id') or 'profile'
    fields = request.rel_url.query.get('fields') or None
    if fields is not None:
        fields = fields.split(',')

    async with request.app['db'].acquire() as connection:
        user = await account_func.get_account_by_id(connection,
                                                    user_id=user_id,
                                                    service_id=service_id,
                                                    fields=fields)

    if user is None:
        raise InvalidParameter(param_name='user_id or service_id')

    return format_account(account=user)
