import aiohttp.web

from account.db.json_formatters import format_account
import account.functions.account as account_func


async def search_users(request: aiohttp.web.Request):
    """
    ---
    summary: Поиск пользователей по указанным параметрам
    tags:
        - User
    produces:
        - application/json
    parameters:
        - name: nickname
          in: query
          type: string
          description: Никнейм пользователя.
          required: false
        - name: email
          in: query
          type: string
          description: Адрес электронной почты пользователя.
          required: false
        - name: phone
          in: query
          type: string
          description: Номер телефона пользователя.
          required: false
        - name: vk_id
          in: query
          type: string
          description: Идентификатор пользователя в социальной сети ВКонтакте.
          required: false
        - name: twitter_id
          in: query
          type: string
          description: Идентификатор пользователя в социальной сети Twitter.
          required: false
        - name: facebook_id
          in: query
          type: string
          description: Идентификатор пользователя в социальной сети Facebook.
          required: false
        - name: google_id
          in: query
          type: string
          description: Идентификатор пользователя в сервисах Google.
          required: false
        - name: fields
          in: query
          type: string
          required: false
          description: Список полей, перечисленных через запятую, которые требуется вернуть.
    responses:
        200:
            description: OK
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: true
                    data:
                        type: object
                        properties:
                            users:
                                type: array
                                description: Список пользователей.
                                items:
                                    type: object
                                    properties:
                                        user_id:
                                            type: integer
                                            description: Основной идентификатор пользователя.
                                        shop_user_id:
                                            type: integer
                                            description: Идентификатор пользователя в сервисе shop.
                                            x-nullable: true
                                        forum_user_id:
                                            type: integer
                                            description: Идентификатор пользователя в сервисе forum.
                                            x-nullable: true
                                        nickname:
                                            type: string
                                            description: Никнейм пользователя.
                                        name:
                                            type: string
                                            description: Имя пользователя.
                                            x-nullable: true
                                        surname:
                                            type: string
                                            description: Фамилия пользователя.
                                            x-nullable: true
                                        email:
                                            type: string
                                            description: Адрес электронной почты пользователя.
                                        email_is_verified:
                                            type: boolean
                                            description: Флаг, показывающий верифицирован ли email пользователя (true)
                                                         или нет (false).
                                        phone:
                                            type: string
                                            description: Номер телефона пользователя.
                                            x-nullable: true
                                        phone_is_verified:
                                            type: boolean
                                            description: Флаг, показывающий верифицирован ли номер телефона
                                                         пользователя (true) или нет (false).
                                        password:
                                            type: string
                                            description: Хэш пароля пользователя.
                                        vk_id:
                                            type: string
                                            description: Идентификатор пользователя в социальной сети ВКонтакте.
                                            x-nullable: true
                                        twitter_id:
                                            type: string
                                            description: Идентификатор пользователя в социальной сети Twitter.
                                            x-nullable: true
                                        facebook_id:
                                            type: string
                                            description: Идентификатор пользователя в социальной сети Facebook.
                                            x-nullable: true
                                        google_id:
                                            type: string
                                            description: Идентификатор пользователя в сервисах Google.
                                            x-nullable: true
                                        gender:
                                            type: string
                                            description: Пол пользователя.
                                        birth_date:
                                            type: string
                                            description: Дата рождения пользователя в ISO формате.
                                        social:
                                            type: object
                                            description: Словарь с юзер неймами пользователя в мессенджерах.
                                        image_large:
                                            type: string
                                            description: Ссылка (без хоста) на аватар пользователя (большой).
                                            x-nullable: true
                                        image_small:
                                            type: string
                                            description: Ссылка (без хоста) на аватар пользователя (маленький).
                                            x-nullable: true
                                        created_at:
                                            type: integer
                                            description: Дата и время регистрации пользователя в ISO формате.
                        required:
                            - users
                required:
                    - success
                    - data
        400:
            description: Bad Request
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: false
                    error:
                        type: object
                        properties:
                            code:
                                type: integer
                                description: Код ошибки.
                                example: 1000
                            message:
                                type: string
                                description: Описание ошибки.
                                example: Internal server error
                        required:
                            - code
                            - message
                required:
                    - success
                    - error
    """
    filters = dict()
    nickname = request.rel_url.query.get('nickname')
    if nickname:
        filters['nickname_lower'] = nickname.lower()
    email = request.rel_url.query.get('email')
    if email:
        filters['email'] = email.lower()
    phone = request.rel_url.query.get('phone')
    if phone:
        filters['phone'] = phone
    vk_id = request.rel_url.query.get('vk_id')
    if vk_id:
        filters['vk_id'] = vk_id
    twitter_id = request.rel_url.query.get('twitter_id')
    if twitter_id:
        filters['twitter_id'] = twitter_id
    facebook_id = request.rel_url.query.get('facebook_id')
    if facebook_id:
        filters['facebook_id'] = facebook_id
    google_id = request.rel_url.query.get('google_id')
    if google_id:
        filters['google_id'] = google_id

    fields = request.rel_url.query.get('fields') or None
    if fields is not None:
        fields = fields.split(',')

    async with request.app['db'].acquire() as connection:
        users = await account_func.search_users(connection,
                                                fields=fields,
                                                filters=filters)
    users = [format_account(account=user) for user in users]

    return {
        'users': users
    }
