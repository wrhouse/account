from datetime import datetime
import hashlib

import aiohttp.web

from account.db.enums import GenderTypes
import account.functions.account as account_func
from account.responses import DuplicateError
from account.utils.images import create_and_upload_image, remove_from_s3


async def update_user(request: aiohttp.web.Request):
    """
    ---
    summary: Создание или обновление информации о пользователе
    tags:
        - User
    produces:
        - application/json
    parameters:
        - name: user_id
          in: path
          type: integer
          required: true
          minimum: 1
          description: Основной идентификатор пользователя.
        - name: user
          in: body
          required: true
          schema:
            type: object
            properties:
                shop_user_id:
                    type: integer
                    description: Идентификатор пользователя в сервисе shop.
                forum_user_id:
                    type: integer
                    description: Идентификатор пользователя в сервисе forum.
                nickname:
                    type: string
                    description: Никнейм пользователя.
                    minLength: 3
                name:
                    type: string
                    description: Имя пользователя.
                    minLength: 2
                surname:
                    type: string
                    description: Фамилия пользователя.
                    minLength: 2
                email:
                    type: string
                    description: Адрес электронной почты пользователя.
                    format: email
                phone:
                    type: string
                    description: Номер телефона пользователя.
                password:
                    type: string
                    description: Хэш пароля пользователя.
                vk_id:
                    type: string
                    description: Идентификатор пользователя в социальной сети ВКонтакте.
                twitter_id:
                    type: string
                    description: Идентификатор пользователя в социальной сети Twitter.
                facebook_id:
                    type: string
                    description: Идентификатор пользователя в социальной сети Facebook.
                google_id:
                    type: string
                    description: Идентификатор пользователя в сервисах Google.
                gender:
                    type: string
                    description: Пол пользователя.
                birth_date:
                    type: string
                    description: Дата рождения пользователя.
                    format: date
                social:
                    type: object
                    description: Словарь с юзер неймами пользователя в мессенджерах.
                image:
                    type: string
                    description: Аватар пользователя в формате base64..
    responses:
        200:
            description: OK
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: true
                required:
                    - success
        400:
            description: Bad Request
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: false
                    error:
                        type: object
                        properties:
                            code:
                                type: integer
                                description: Код ошибки.
                                example: 1000
                            message:
                                type: string
                                description: Описание ошибки.
                                example: Internal server error
                        required:
                            - code
                            - message
                required:
                    - success
                    - error
    """
    user_id = int(request.match_info['user_id'])
    request_data = await request.json()

    shop_user_id = request_data.get('shop_user_id')
    forum_user_id = request_data.get('forum_user_id')

    nickname = request_data.get('nickname')
    name = request_data.get('name')
    surname = request_data.get('surname')
    password = request_data.get('password')

    email = request_data.get('email')
    email_is_verified = None
    if email is not None:
        email_is_verified = False

    phone = request_data.get('phone')
    phone_is_verified = None
    if phone is not None:
        phone_is_verified = False

    vk_id = request_data.get('vk_id')
    twitter_id = request_data.get('twitter_id')
    facebook_id = request_data.get('facebook_id')
    google_id = request_data.get('google_id')

    social = request_data.get('social')

    gender = request_data.get('gender')
    if gender is not None:
        gender = GenderTypes(gender)

    birth_date = request_data.get('birth_date')
    if birth_date is not None:
        birth_date = datetime.fromisoformat(birth_date).date()

    image_large, image_small = (None, None)
    image = request_data.get('image')
    if image is not None:
        if len(image) > 0:
            filename = hashlib.md5(f'user:{user_id}'.encode('utf-8')).hexdigest()
            image_large = await create_and_upload_image(base64_image=image,
                                                        size=request.app['settings'].user_image_large_size,
                                                        directory=request.app['settings'].user_image_large_directory,
                                                        filename=filename)
            image_small = await create_and_upload_image(base64_image=image,
                                                        size=request.app['settings'].user_image_small_size,
                                                        directory=request.app['settings'].user_image_small_directory,
                                                        filename=filename)

    async with request.app['db'].acquire() as connection:
        if nickname is not None:
            exists_nickname = await account_func.is_exists(connection,
                                                           nickname=nickname)
            if exists_nickname:
                raise DuplicateError(param_name='nickname')

        if email is not None:
            exists_email = await account_func.is_exists(connection,
                                                        email=email)
            if exists_email:
                raise DuplicateError(param_name='email')

        if vk_id:
            exists_vk_id = await account_func.is_exists(connection,
                                                        vk_id=vk_id)
            if exists_vk_id:
                raise DuplicateError(param_name='vk_id')

        if twitter_id:
            exists_twitter_id = await account_func.is_exists(connection,
                                                             twitter_id=twitter_id)
            if exists_twitter_id:
                raise DuplicateError(param_name='twitter_id')

        if facebook_id:
            exists_facebook_id = await account_func.is_exists(connection,
                                                              facebook_id=facebook_id)
            if exists_facebook_id:
                raise DuplicateError(param_name='facebook_id')

        if google_id:
            exists_google_id = await account_func.is_exists(connection,
                                                            google_id=google_id)
            if exists_google_id:
                raise DuplicateError(param_name='google_id')

        user_id = await account_func.update(connection,
                                            user_id=user_id,
                                            shop_user_id=shop_user_id,
                                            forum_user_id=forum_user_id,
                                            nickname=nickname,
                                            name=name,
                                            surname=surname,
                                            email=email,
                                            email_is_verified=email_is_verified,
                                            phone=phone,
                                            phone_is_verified=phone_is_verified,
                                            password=password,
                                            vk_id=vk_id,
                                            twitter_id=twitter_id,
                                            facebook_id=facebook_id,
                                            google_id=google_id,
                                            gender=gender,
                                            birth_date=birth_date,
                                            social=social,
                                            image_large=image_large,
                                            image_small=image_small)
