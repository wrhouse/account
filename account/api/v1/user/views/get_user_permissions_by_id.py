import aiohttp.web

import account.functions.account as account_func


async def get_user_permissions_by_id(request: aiohttp.web.Request):
    """
    ---
    summary: Получение списка прав, которыми обладает пользователь
    tags:
        - User
    produces:
        - application/json
    parameters:
        - name: user_id
          in: path
          type: integer
          required: true
          minimum: 1
          description: Идентификатор пользователя, права которого требуется вернуть.
        - name: service_id
          in: query
          type: string
          required: false
          default: profile
          description: Идентификатор сервиса, которому принадлежит user_id.
        - name: show_group_permissions
          in: query
          type: integer
          required: false
          minimum: 0
          maximum: 1
          default: 0
          description: Флаг, отвечающий за отображение списка прав пользователя, которыми он обладает, поскольку
                       принадлежит определенной группе (true - показывать права групп, false - нет).
    responses:
        200:
            description: OK
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: true
                    data:
                        type: object
                        properties:
                            permission_ids:
                                type: array
                                description: Массив с идентификаторами прав, которыми обладает пользователь.
                                items:
                                    type: string
                        required:
                            - permission_ids
                required:
                    - success
                    - data
        400:
            description: Bad Request
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: false
                    error:
                        type: object
                        properties:
                            code:
                                type: integer
                                description: Код ошибки.
                                example: 1000
                            message:
                                type: string
                                description: Описание ошибки.
                                example: Internal server error
                        required:
                            - code
                            - message
                required:
                    - success
                    - error
    """
    user_id = int(request.match_info['user_id'])
    service_id = request.rel_url.query.get('service_id') or 'profile'
    show_group_permissions = bool(int(request.rel_url.query.get('show_group_permissions') or 0))

    async with request.app['db'].acquire() as connection:
        user = await account_func.get_account_by_id(connection,
                                                    user_id=user_id,
                                                    service_id=service_id,
                                                    fields=['user_id'])
        permission_ids = await account_func.get_account_permission_ids(connection,
                                                                       user_id=user.user_id)
        if show_group_permissions:
            group_permission_ids = await account_func.get_account_group_permission_ids(connection,
                                                                                       user_id=user.user_id)
            permission_ids = list(set(permission_ids).union(group_permission_ids))

    return {
        'permission_ids': permission_ids
    }
