import aiohttp.web

from account.db.enums import GenderTypes
import account.functions.account as account_func
from account.responses import DuplicateError


async def create_user(request: aiohttp.web.Request):
    """
    ---
    summary: Создание нового пользователя
    tags:
        - User
    produces:
        - application/json
    parameters:
        - name: user
          in: body
          required: true
          schema:
            type: object
            properties:
                nickname:
                    type: string
                    description: Никнейм пользователя.
                    minLength: 3
                name:
                    type: string
                    description: Имя пользователя.
                    minLength: 2
                surname:
                    type: string
                    description: Фамилия пользователя.
                    minLength: 2
                email:
                    type: string
                    description: Адрес электронной почты пользователя.
                    format: email
                password:
                    type: string
                    description: Хэш пароля пользователя.
                vk_id:
                    type: string
                    description: Идентификатор пользователя в социальной сети ВКонтакте.
                twitter_id:
                    type: string
                    description: Идентификатор пользователя в социальной сети Twitter.
                facebook_id:
                    type: string
                    description: Идентификатор пользователя в социальной сети Facebook.
                google_id:
                    type: string
                    description: Идентификатор пользователя в сервисах Google.
            required:
                - nickname
                - email
                - password
    responses:
        200:
            description: OK
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: true
                    data:
                        type: object
                        properties:
                            user_id:
                                type: integer
                                description: Основной идентификатор пользователя
                        required:
                            - user_id
                required:
                    - success
                    - data
        400:
            description: Bad Request
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: false
                    error:
                        type: object
                        properties:
                            code:
                                type: integer
                                description: Код ошибки.
                                example: 1000
                            message:
                                type: string
                                description: Описание ошибки.
                                example: Internal server error
                        required:
                            - code
                            - message
                required:
                    - success
                    - error
    """
    request_data = await request.json()

    nickname = request_data['nickname']
    name = request_data.get('name')
    surname = request_data.get('surname')
    email = request_data['email']
    password = request_data['password']

    vk_id = request_data.get('vk_id') or ''
    twitter_id = request_data.get('twitter_id') or ''
    facebook_id = request_data.get('facebook_id') or ''
    google_id = request_data.get('google_id') or ''

    async with request.app['db'].acquire() as connection:
        exists_nickname = await account_func.is_exists(connection,
                                                       nickname=nickname)
        if exists_nickname:
            raise DuplicateError(param_name='nickname')
        
        exists_email = await account_func.is_exists(connection,
                                                    email=email)
        if exists_email:
            raise DuplicateError(param_name='email')
        
        if vk_id:
            exists_vk_id = await account_func.is_exists(connection,
                                                        vk_id=vk_id)
            if exists_vk_id:
                raise DuplicateError(param_name='vk_id')
        
        if twitter_id:
            exists_twitter_id = await account_func.is_exists(connection,
                                                             twitter_id=twitter_id)
            if exists_twitter_id:
                raise DuplicateError(param_name='twitter_id')

        if facebook_id:
            exists_facebook_id = await account_func.is_exists(connection,
                                                              facebook_id=facebook_id)
            if exists_facebook_id:
                raise DuplicateError(param_name='facebook_id')

        if google_id:
            exists_google_id = await account_func.is_exists(connection,
                                                            google_id=google_id)
            if exists_google_id:
                raise DuplicateError(param_name='google_id')

        user_id = await account_func.create(connection,
                                            nickname=nickname,
                                            name=name,
                                            surname=surname,
                                            email=email,
                                            password=password,
                                            vk_id=vk_id,
                                            twitter_id=twitter_id,
                                            facebook_id=facebook_id,
                                            google_id=google_id,
                                            gender=GenderTypes.undefined)
    
    return {
        'user_id': user_id
    }

