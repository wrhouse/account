import aiohttp.web

from account.db.json_formatters import format_account
import account.functions.account as account_func


async def get_users_list(request: aiohttp.web.Request):
    """
    ---
    summary: Получение списка пользователей
    tags:
        - User
    produces:
        - application/json
    parameters:
        - name: count
          in: query
          type: integer
          required: false
          minimum: 1
          maximum: 200
          default: 20
          description: Количество пользователей, которое требуется вернуть
        - name: offset
          in: query
          type: integer
          required: false
          minimum: 0
          default: 0
          description: Отступ, с которым требуется вернуть список пользователей.
        - name: rev
          in: query
          type: integer
          required: false
          default: 0
          minimum: 0
          maximum: 1
          description: Флаг, отвечающий за порядок, в котором требуется вернуть список пользователей (0 - в обратном
                       хронологическом порядке, 1 - в хронологическом порядке (по умолчанию)).
        - name: fields
          in: query
          type: string
          required: false
          description: Список полей, перечисленных через запятую, которые требуется вернуть
    responses:
        200:
            description: OK
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: true
                    data:
                        type: object
                        properties:
                            users:
                                type: array
                                description: Список пользователей.
                                items:
                                    type: object
                                    properties:
                                        user_id:
                                            type: integer
                                            description: Основной идентификатор пользователя.
                                        shop_user_id:
                                            type: integer
                                            description: Идентификатор пользователя в сервисе shop.
                                            x-nullable: true
                                        forum_user_id:
                                            type: integer
                                            description: Идентификатор пользователя в сервисе forum.
                                            x-nullable: true
                                        nickname:
                                            type: string
                                            description: Никнейм пользователя.
                                        name:
                                            type: string
                                            description: Имя пользователя.
                                            x-nullable: true
                                        surname:
                                            type: string
                                            description: Фамилия пользователя.
                                            x-nullable: true
                                        email:
                                            type: string
                                            description: Адрес электронной почты пользователя.
                                        email_is_verified:
                                            type: boolean
                                            description: Флаг, показывающий верифицирован ли email пользователя (true)
                                                         или нет (false).
                                        phone:
                                            type: string
                                            description: Номер телефона пользователя.
                                            x-nullable: true
                                        phone_is_verified:
                                            type: boolean
                                            description: Флаг, показывающий верифицирован ли номер телефона
                                                         пользователя (true) или нет (false).
                                        password:
                                            type: string
                                            description: Хэш пароля пользователя.
                                        vk_id:
                                            type: string
                                            description: Идентификатор пользователя в социальной сети ВКонтакте.
                                            x-nullable: true
                                        twitter_id:
                                            type: string
                                            description: Идентификатор пользователя в социальной сети Twitter.
                                            x-nullable: true
                                        facebook_id:
                                            type: string
                                            description: Идентификатор пользователя в социальной сети Facebook.
                                            x-nullable: true
                                        google_id:
                                            type: string
                                            description: Идентификатор пользователя в сервисах Google.
                                            x-nullable: true
                                        gender:
                                            type: string
                                            description: Пол пользователя.
                                        birth_date:
                                            type: string
                                            description: Дата рождения пользователя в ISO формате.
                                        social:
                                            type: object
                                            description: Словарь с юзер неймами пользователя в мессенджерах.
                                        image_large:
                                            type: string
                                            description: Ссылка (без хоста) на аватар пользователя (большой).
                                            x-nullable: true
                                        image_small:
                                            type: string
                                            description: Ссылка (без хоста) на аватар пользователя (маленький).
                                            x-nullable: true
                                        created_at:
                                            type: integer
                                            description: Дата и время регистрации пользователя в ISO формате.
                        required:
                            - users
                required:
                    - success
                    - data
        400:
            description: Bad Request
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: false
                    error:
                        type: object
                        properties:
                            code:
                                type: integer
                                description: Код ошибки.
                                example: 1000
                            message:
                                type: string
                                description: Описание ошибки.
                                example: Internal server error
                        required:
                            - code
                            - message
                required:
                    - success
                    - error
    """
    count = int(request.rel_url.query.get('count') or 20)
    offset = int(request.rel_url.query.get('offset') or 0)
    rev = int(request.rel_url.query.get('rev') or 1)
    fields = request.rel_url.query.get('fields') or None
    if fields is not None:
        fields = fields.split(',')

    async with request.app['db'].acquire() as connection:
        users = await account_func.get_list(connection,
                                            count=count,
                                            offset=offset,
                                            rev=rev,
                                            fields=fields)
    users = [format_account(account=user) for user in users]

    return {
        'users': users
    }
