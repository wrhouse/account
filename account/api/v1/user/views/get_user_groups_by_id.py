import aiohttp.web

import account.functions.account as account_func


async def get_user_groups_by_id(request: aiohttp.web.Request):
    """
    ---
    summary: Получение списка групп, которым принадлежит пользователь
    tags:
        - User
    produces:
        - application/json
    parameters:
        - name: user_id
          in: path
          type: integer
          required: true
          minimum: 1
          description: Идентификатор пользователя, группы которого требуется вернуть.
        - name: service_id
          in: query
          type: string
          required: false
          default: profile
          description: Идентификатор сервиса, которому принадлежит user_id.
    responses:
        200:
            description: OK
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: true
                    data:
                        type: object
                        properties:
                            group_ids:
                                type: array
                                description: Массив с идентификаторами групп, которым принадлежит пользователь.
                                items:
                                    type: string
                        required:
                            - group_ids
                required:
                    - success
                    - data
        400:
            description: Bad Request
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: false
                    error:
                        type: object
                        properties:
                            code:
                                type: integer
                                description: Код ошибки.
                                example: 1000
                            message:
                                type: string
                                description: Описание ошибки.
                                example: Internal server error
                        required:
                            - code
                            - message
                required:
                    - success
                    - error
    """
    user_id = int(request.match_info['user_id'])
    service_id = request.rel_url.query.get('service_id') or 'profile'

    async with request.app['db'].acquire() as connection:
        account_groups = await account_func.get_account_groups(connection,
                                                               user_id=user_id,
                                                               service_id=service_id)
    group_ids = [account_group.group_id for account_group in account_groups]

    return {
        'group_ids': group_ids
    }
