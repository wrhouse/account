import aiohttp.web

import account.functions.account as account_func


async def verify_email(request: aiohttp.web.Request):
    """
    ---
    summary: Верификация адреса электронной почты пользователя
    tags:
        - User
    produces:
        - application/json
    parameters:
        - name: user_id
          in: path
          type: integer
          required: true
          minimum: 1
          description: Идентификатор пользователя, электронную почту которого требуется верифицировать.
    responses:
        200:
            description: OK
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: true
                required:
                    - success
        400:
            description: Bad Request
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: false
                    error:
                        type: object
                        properties:
                            code:
                                type: integer
                                description: Код ошибки.
                                example: 1000
                            message:
                                type: string
                                description: Описание ошибки.
                                example: Internal server error
                        required:
                            - code
                            - message
                required:
                    - success
                    - error
    """
    user_id = int(request.match_info['user_id'])

    async with request.app['db'].acquire() as connection:
        await account_func.update(connection,
                                  user_id=user_id,
                                  email_is_verified=True)
