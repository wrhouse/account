import aiohttp.web

from account.api.v1.permission.views.add_permission_for_group import add_permission_for_group
from account.api.v1.permission.views.add_permission_for_user import add_permission_for_user
from account.api.v1.permission.views.create_or_update_permission import create_or_update_permission
from account.api.v1.permission.views.delete_permission import delete_permission
from account.api.v1.permission.views.delete_permission_from_group import delete_permission_from_group
from account.api.v1.permission.views.delete_permission_from_user import delete_permission_from_user
from account.api.v1.permission.views.get_permissions_list import get_permissions_list


def init_routes(app: aiohttp.web.Application, prefix):
    app.router.add_get(prefix, get_permissions_list)
    app.router.add_post(prefix + r'/{permission_id:\w+}', create_or_update_permission)
    app.router.add_delete(prefix + r'/{permission_id:\w+}', delete_permission)
    app.router.add_post(prefix + r'/{permission_id:\w+}/users/{user_id:\d+}', add_permission_for_user)
    app.router.add_delete(prefix + r'/{permission_id:\w+}/users/{user_id:\d+}', delete_permission_from_user)
    app.router.add_post(prefix + r'/{permission_id:\w+}/groups/{group_id:\w+}', add_permission_for_group)
    app.router.add_delete(prefix + r'/{permission_id:\w+}/groups/{group_id:\w+}', delete_permission_from_group)
