import aiohttp.web

import account.functions.group as group_func
import account.functions.permission as permission_func
import account.functions.group_permissions as group_permissions_func
from account.responses import InvalidParameter


async def delete_permission_from_group(request: aiohttp.web.Request):
    """
    ---
    summary: Убрать права для группы пользователей
    tags:
        - Permission
    produces:
        - application/json
    parameters:
        - name: permission_id
          in: path
          type: string
          required: true
          description: Идентификатор права, которое требуется убрать для группы пользователей.
        - name: group_id
          in: path
          type: string
          required: true
          description: Идентификатор группы пользователей.
    responses:
        200:
            description: OK
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: true
                required:
                    - success
        400:
            description: Bad Request
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: false
                    error:
                        type: object
                        properties:
                            code:
                                type: integer
                                description: Код ошибки.
                                example: 1000
                            message:
                                type: string
                                description: Описание ошибки.
                                example: Internal server error
                        required:
                            - code
                            - message
                required:
                    - success
                    - error
    """
    permission_id = request.match_info['permission_id']
    group_id = request.match_info['group_id']

    async with request.app['db'].acquire() as connection:
        permission_exists = await permission_func.is_exists(connection,
                                                            permission_id=permission_id)
        if not permission_exists:
            raise InvalidParameter(param_name='permission_id')

        group_exists = await group_func.is_exists(connection,
                                                  group_id=group_id)
        if not group_exists:
            raise InvalidParameter(param_name='group_id')

        status = await group_permissions_func.delete(connection,
                                                     group_id=group_id,
                                                     permission_id=permission_id)
        if not status:
            raise InvalidParameter(param_name='permission_id or group_id')

    return None
