import aiohttp.web

import account.functions.account as account_func
import account.functions.permission as permission_func
import account.functions.account_permissions as account_permissions_func
from account.responses import InvalidParameter, DuplicateError


async def add_permission_for_user(request: aiohttp.web.Request):
    """
    ---
    summary: Выдать права пользователю
    tags:
        - Permission
    produces:
        - application/json
    parameters:
        - name: permission_id
          in: path
          type: string
          required: true
          description: Идентификатор права, которое требуется выдать пользователю.
        - name: user_id
          in: path
          type: integer
          required: true
          minimum: 1
          description: Идентификатор пользователя.
    responses:
        200:
            description: OK
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: true
                required:
                    - success
        400:
            description: Bad Request
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: false
                    error:
                        type: object
                        properties:
                            code:
                                type: integer
                                description: Код ошибки.
                                example: 1000
                            message:
                                type: string
                                description: Описание ошибки.
                                example: Internal server error
                        required:
                            - code
                            - message
                required:
                    - success
                    - error
    """
    permission_id = request.match_info['permission_id']
    user_id = int(request.match_info['user_id'])

    async with request.app['db'].acquire() as connection:
        permission_exists = await permission_func.is_exists(connection,
                                                            permission_id=permission_id)
        if not permission_exists:
            raise InvalidParameter(param_name='permission_id')

        account_exists = await account_func.is_exists(connection,
                                                      user_id=user_id)
        if not account_exists:
            raise InvalidParameter(param_name='user_id')

        account_permissions_exists = await account_permissions_func.is_exists(connection,
                                                                              user_id=user_id,
                                                                              permission_id=permission_id)
        if account_permissions_exists:
            raise DuplicateError(param_name='user_id and permission_id')

        await account_permissions_func.create(connection,
                                              user_id=user_id,
                                              permission_id=permission_id)

    return None
