import aiohttp.web

from account.db.json_formatters import format_permission
import account.functions.permission as permission_func


async def get_permissions_list(request: aiohttp.web.Request):
    """
    ---
    summary: Получение списка прав, доступных в системе
    tags:
        - Permission
    produces:
        - application/json
    parameters:
        - name: fields
          in: query
          type: string
          required: false
          description: Список полей, перечисленных через запятую, которые требуется вернуть
    responses:
        200:
            description: OK
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: true
                    data:
                        type: object
                        properties:
                            permissions:
                                type: array
                                description: Массив с информацией о правах доступа.
                                items:
                                    type: object
                                    properties:
                                        permission_id:
                                            type: string
                                            description: Идентификатор прав доступа.
                                        description:
                                            type: string
                                            description: Описание прав доступа.
                                            x-nullable: true
                                    required:
                                        - permission_id
                                        - description
                        required:
                            - permissions
                required:
                    - success
                    - data
        400:
            description: Bad Request
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: false
                    error:
                        type: object
                        properties:
                            code:
                                type: integer
                                description: Код ошибки.
                                example: 1000
                            message:
                                type: string
                                description: Описание ошибки.
                                example: Internal server error
                        required:
                            - code
                            - message
                required:
                    - success
                    - error
    """
    fields = request.rel_url.query.get('fields') or None
    if fields is not None:
        fields = fields.split(',')

    async with request.app['db'].acquire() as connection:
        permissions = await permission_func.get_list(connection,
                                                     fields=fields)
    permissions = [format_permission(permission) for permission in permissions]

    return {
        'permissions': permissions
    }
