import aiohttp.web

import account.functions.group as group_func
import account.functions.permission as permission_func
import account.functions.group_permissions as group_permissions_func
from account.responses import InvalidParameter, DuplicateError


async def add_permission_for_group(request: aiohttp.web.Request):
    """
    ---
    summary: Выдать права группе пользователей
    tags:
        - Permission
    produces:
        - application/json
    parameters:
        - name: permission_id
          in: path
          type: string
          required: true
          description: Идентификатор права, которое требуется выдать группе пользователей.
        - name: group_id
          in: path
          type: string
          required: true
          description: Идентификатор группы пользователей.
    responses:
        200:
            description: OK
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: true
                required:
                    - success
        400:
            description: Bad Request
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: false
                    error:
                        type: object
                        properties:
                            code:
                                type: integer
                                description: Код ошибки.
                                example: 1000
                            message:
                                type: string
                                description: Описание ошибки.
                                example: Internal server error
                        required:
                            - code
                            - message
                required:
                    - success
                    - error
    """
    permission_id = request.match_info['permission_id']
    group_id = request.match_info['group_id']

    async with request.app['db'].acquire() as connection:
        permission_exists = await permission_func.is_exists(connection,
                                                            permission_id=permission_id)
        if not permission_exists:
            raise InvalidParameter(param_name='permission_id')

        group_exists = await group_func.is_exists(connection,
                                                  group_id=group_id)
        if not group_exists:
            raise InvalidParameter(param_name='group_id')

        group_permissions_exists = await group_permissions_func.is_exists(connection,
                                                                          group_id=group_id,
                                                                          permission_id=permission_id)
        if group_permissions_exists:
            raise DuplicateError(param_name='group_id and permission_id')

        await group_permissions_func.create(connection,
                                            group_id=group_id,
                                            permission_id=permission_id)

    return None
