import aiohttp.web

import account.functions.permission as permission_func


async def create_or_update_permission(request: aiohttp.web.Request):
    """
    ---
    summary: Создать или обновить права доступа
    tags:
        - Permission
    produces:
        - application/json
    parameters:
        - name: permission_id
          in: path
          type: string
          required: true
          description: Идентификатор прав доступа.
        - name: params
          in: body
          required: false
          schema:
            type: object
            properties:
                description:
                    type: string
                    description: Описание прав доступа.
    responses:
        200:
            description: OK
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: true
                required:
                    - success
        400:
            description: Bad Request
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: false
                    error:
                        type: object
                        properties:
                            code:
                                type: integer
                                description: Код ошибки.
                                example: 1000
                            message:
                                type: string
                                description: Описание ошибки.
                                example: Internal server error
                        required:
                            - code
                            - message
                required:
                    - success
                    - error
    """
    permission_id = request.match_info['permission_id']
    request_data = await request.json()
    description = request_data.get('description')

    async with request.app['db'].acquire() as connection:
        permission_exists = await permission_func.is_exists(connection,
                                                            permission_id=permission_id)
        if permission_exists:
            await permission_func.update(connection,
                                         permission_id=permission_id,
                                         description=description)
        else:
            await permission_func.create(connection,
                                         permission_id=permission_id,
                                         description=description)

    return None
