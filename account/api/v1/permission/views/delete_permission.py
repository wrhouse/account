import aiohttp.web

import account.functions.permission as permission_func
from account.responses import InvalidParameter


async def delete_permission(request: aiohttp.web.Request):
    """
    ---
    summary: Удалить права доступа
    tags:
        - Permission
    produces:
        - application/json
    parameters:
        - name: permission_id
          in: path
          type: string
          required: true
          description: Идентификатор прав доступа, которые требуется удалить.
    responses:
        200:
            description: OK
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: true
                required:
                    - success
        400:
            description: Bad Request
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: false
                    error:
                        type: object
                        properties:
                            code:
                                type: integer
                                description: Код ошибки.
                                example: 1000
                            message:
                                type: string
                                description: Описание ошибки.
                                example: Internal server error
                        required:
                            - code
                            - message
                required:
                    - success
                    - error
    """
    permission_id = request.match_info['permission_id']

    async with request.app['db'].acquire() as connection:
        status = await permission_func.delete(connection,
                                              permission_id=permission_id)
        if not status:
            raise InvalidParameter(param_name='permission_id')

    return None
