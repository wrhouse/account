import aiohttp.web

import account.functions.account as account_func
import account.functions.account_groups as account_groups_func
import account.functions.group as group_func
from account.responses import InvalidParameter


async def delete_user_from_group(request: aiohttp.web.Request):
    """
    ---
    summary: Удаление пользователя из группы пользователей
    tags:
        - Group
    produces:
        - application/json
    parameters:
        - name: group_id
          in: path
          type: string
          required: true
          description: Идентификатор группы пользователей.
        - name: user_id
          in: path
          type: integer
          required: true
          minimum: 1
          description: Идентификатор пользователя.
    responses:
        200:
            description: OK
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: true
                required:
                    - success
        400:
            description: Bad Request
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: false
                    error:
                        type: object
                        properties:
                            code:
                                type: integer
                                description: Код ошибки.
                                example: 1000
                            message:
                                type: string
                                description: Описание ошибки.
                                example: Internal server error
                        required:
                            - code
                            - message
                required:
                    - success
                    - error
    """
    group_id = request.match_info['group_id']
    user_id = int(request.match_info['user_id'])

    async with request.app['db'].acquire() as connection:
        group_exists = await group_func.is_exists(connection,
                                                  group_id=group_id)
        if not group_exists:
            raise InvalidParameter(param_name='group_id')

        account_exists = await account_func.is_exists(connection,
                                                      user_id=user_id)
        if not account_exists:
            raise InvalidParameter(param_name='user_id')

        status = await account_groups_func.delete(connection,
                                                  user_id=user_id,
                                                  group_id=group_id)
        if not status:
            raise InvalidParameter(param_name='group_id or user_id')

    return None
