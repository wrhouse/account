import aiohttp.web

import account.functions.group as group_func


async def create_or_update_group(request: aiohttp.web.Request):
    """
    ---
    summary: Создание или обновление информации о группе пользователей
    tags:
        - Group
    produces:
        - application/json
    parameters:
        - name: group_id
          in: path
          type: string
          required: true
          description: Идентификатор группы пользователей.
        - name: group
          in: body
          required: true
          schema:
            type: object
            properties:
                name:
                    type: string
                    description: Название группы пользователей.
                description:
                    type: string
                    description: Описание группы пользователей.
            required:
                - name
    responses:
        200:
            description: OK
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: true
                required:
                    - success
        400:
            description: Bad Request
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: false
                    error:
                        type: object
                        properties:
                            code:
                                type: integer
                                description: Код ошибки.
                                example: 1000
                            message:
                                type: string
                                description: Описание ошибки.
                                example: Internal server error
                        required:
                            - code
                            - message
                required:
                    - success
                    - error
    """
    group_id = request.match_info['group_id']
    request_data = await request.json()
    name = request_data['name']
    description = request_data.get('description')

    async with request.app['db'].acquire() as connection:
        group_exists = await group_func.is_exists(connection,
                                                  group_id=group_id)
        if group_exists:
            await group_func.update(connection,
                                    group_id=group_id,
                                    name=name,
                                    description=description)
        else:
            await group_func.create(connection,
                                    group_id=group_id,
                                    name=name,
                                    description=description)

    return None
