import aiohttp.web

from account.db.json_formatters import format_group
import account.functions.group as group_func


async def get_groups_list(request: aiohttp.web.Request):
    """
    ---
    summary: Получение списка групп пользователей
    tags:
        - Group
    produces:
        - application/json
    parameters:
        - name: fields
          in: query
          type: string
          required: false
          description: Список полей, перечисленных через запятую, которые требуется вернуть
    responses:
        200:
            description: OK
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: true
                    data:
                        type: object
                        properties:
                            groups:
                                type: array
                                description: Массив с информацией о группах пользователей.
                                items:
                                    type: object
                                    properties:
                                        group_id:
                                            type: string
                                            description: Идентификатор группы пользователей.
                                        name:
                                            type: string
                                            description: Название группы пользователей.
                                        description:
                                            type: string
                                            description: Описание группы пользователей.
                                            x-nullable: true
                                        created_at:
                                            type: string
                                            description: Дата и время создания группы пользователей в ISO формате.
                        required:
                            - groups
                required:
                    - success
                    - data
        400:
            description: Bad Request
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: false
                    error:
                        type: object
                        properties:
                            code:
                                type: integer
                                description: Код ошибки.
                                example: 1000
                            message:
                                type: string
                                description: Описание ошибки.
                                example: Internal server error
                        required:
                            - code
                            - message
                required:
                    - success
                    - error
    """
    fields = request.rel_url.query.get('fields') or None
    if fields is not None:
        fields = fields.split(',')

    async with request.app['db'].acquire() as connection:
        groups = await group_func.get_list(connection,
                                           fields=fields)
    groups = [format_group(group) for group in groups]

    return {
        'groups': groups
    }
