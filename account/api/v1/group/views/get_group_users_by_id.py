import aiohttp.web

import account.functions.account_groups as account_groups_func
import account.functions.group as group_func
from account.responses import InvalidParameter


async def get_group_users_by_id(request: aiohttp.web.Request):
    """
    ---
    summary: Получение списка пользователей, принадлежащих группе
    tags:
        - Group
    produces:
        - application/json
    parameters:
        - name: group_id
          in: path
          type: string
          required: true
          description: Идентификатор группы пользователей.
    responses:
        200:
            description: OK
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: true
                    data:
                        type: object
                        properties:
                            user_ids:
                                type: array
                                description: Массив идентификаторов пользователей.
                                items:
                                    type: integer
                        required:
                            - user_ids
                required:
                    - success
                    - data
        400:
            description: Bad Request
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: false
                    error:
                        type: object
                        properties:
                            code:
                                type: integer
                                description: Код ошибки.
                                example: 1000
                            message:
                                type: string
                                description: Описание ошибки.
                                example: Internal server error
                        required:
                            - code
                            - message
                required:
                    - success
                    - error
    """
    group_id = request.match_info['group_id']

    async with request.app['db'].acquire() as connection:
        group_exists = await group_func.is_exists(connection,
                                                  group_id=group_id)
        if not group_exists:
            raise InvalidParameter(param_name='group_id')

        account_groups = await account_groups_func.get_list(connection,
                                                            group_id=group_id)
    user_ids = [account_group.user_id for account_group in account_groups]

    return {
        'user_ids': user_ids
    }
