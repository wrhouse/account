import aiohttp.web

import account.functions.account as account_func
import account.functions.account_groups as account_groups_func
import account.functions.group as group_func
from account.responses import InvalidParameter, DuplicateError


async def add_user_to_group(request: aiohttp.web.Request):
    """
    ---
    summary: Добавить пользователя в группу пользователей
    tags:
        - Group
    produces:
        - application/json
    parameters:
        - name: group_id
          in: path
          type: string
          required: true
          description: Идентификатор группы пользователей.
        - name: user_id
          in: path
          type: integer
          required: true
          minimum: 1
          description: Идентификатор пользователя.
    responses:
        200:
            description: OK
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: true
                required:
                    - success
        400:
            description: Bad Request
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: false
                    error:
                        type: object
                        properties:
                            code:
                                type: integer
                                description: Код ошибки.
                                example: 1000
                            message:
                                type: string
                                description: Описание ошибки.
                                example: Internal server error
                        required:
                            - code
                            - message
                required:
                    - success
                    - error
    """
    group_id = request.match_info['group_id']
    user_id = int(request.match_info['user_id'])

    async with request.app['db'].acquire() as connection:
        group_exists = await group_func.is_exists(connection,
                                                  group_id=group_id)
        if not group_exists:
            raise InvalidParameter(param_name='group_id')

        account_exists = await account_func.is_exists(connection,
                                                      user_id=user_id)
        if not account_exists:
            raise InvalidParameter(param_name='user_id')

        account_groups_exists = await account_groups_func.is_exists(connection,
                                                                    user_id=user_id,
                                                                    group_id=group_id)
        if account_groups_exists:
            raise DuplicateError(param_name='group_id and user_id')

        await account_groups_func.create(connection,
                                         user_id=user_id,
                                         group_id=group_id)

    return None
