import aiohttp.web

import account.functions.group as group_func
from account.responses import InvalidParameter


async def delete_group(request: aiohttp.web.Request):
    """
    ---
    summary: Удаление группы пользователей
    tags:
        - Group
    produces:
        - application/json
    parameters:
        - name: group_id
          in: path
          type: string
          required: true
          description: Идентификатор группы пользователей.
    responses:
        200:
            description: OK
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: true
                required:
                    - success
        400:
            description: Bad Request
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: false
                    error:
                        type: object
                        properties:
                            code:
                                type: integer
                                description: Код ошибки.
                                example: 1000
                            message:
                                type: string
                                description: Описание ошибки.
                                example: Internal server error
                        required:
                            - code
                            - message
                required:
                    - success
                    - error
    """
    group_id = request.match_info['group_id']

    async with request.app['db'].acquire() as connection:
        status = await group_func.delete(connection,
                                         group_id=group_id)
        if not status:
            raise InvalidParameter(param_name='group_id')

    return None
