import aiohttp.web

import account.functions.group as group_func
import account.functions.group_permissions as group_permissions_func
from account.responses import InvalidParameter


async def get_group_permissions_by_id(request: aiohttp.web.Request):
    """
    ---
    summary: Получение списка прав, которыми обладает группа пользователей
    tags:
        - Group
    produces:
        - application/json
    parameters:
        - name: group_id
          in: path
          type: string
          required: true
          description: Идентификатор группы пользователей.
    responses:
        200:
            description: OK
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: true
                    data:
                        type: object
                        properties:
                            permission_ids:
                                type: array
                                description: Массив идентификаторов прав доступа.
                                items:
                                    type: string
                        required:
                            - permission_ids
                required:
                    - success
                    - data
        400:
            description: Bad Request
            schema:
                type: object
                properties:
                    success:
                        type: boolean
                        description: Флаг, обозначающий успех выполнения запроса (true) или неудачу (false).
                        example: false
                    error:
                        type: object
                        properties:
                            code:
                                type: integer
                                description: Код ошибки.
                                example: 1000
                            message:
                                type: string
                                description: Описание ошибки.
                                example: Internal server error
                        required:
                            - code
                            - message
                required:
                    - success
                    - error
    """
    group_id = request.match_info['group_id']

    async with request.app['db'].acquire() as connection:
        group_exists = await group_func.is_exists(connection,
                                                  group_id=group_id)
        if not group_exists:
            raise InvalidParameter(param_name='group_id')

        group_permissions = await group_permissions_func.get_list(connection,
                                                                  group_id=group_id)
    permission_ids = [group_permission.permission_id for group_permission in group_permissions]

    return {
        'permission_ids': permission_ids
    }
