import aiohttp.web

from account.api.v1.group.views.add_user_to_group import add_user_to_group
from account.api.v1.group.views.create_or_update_group import create_or_update_group
from account.api.v1.group.views.delete_group import delete_group
from account.api.v1.group.views.delete_user_from_group import delete_user_from_group
from account.api.v1.group.views.get_groups_list import get_groups_list
from account.api.v1.group.views.get_group_permissions_by_id import get_group_permissions_by_id
from account.api.v1.group.views.get_group_users_by_id import get_group_users_by_id


def init_routes(app: aiohttp.web.Application, prefix):
    app.router.add_get(prefix, get_groups_list)
    app.router.add_post(prefix + r'/{group_id:\w+}', create_or_update_group)
    app.router.add_delete(prefix + r'/{group_id:\w+}', delete_group)
    app.router.add_get(prefix + r'/{group_id:\w+}/users', get_group_users_by_id)
    app.router.add_post(prefix + r'/{group_id:\w+}/users/{user_id:\d+}', add_user_to_group)
    app.router.add_delete(prefix + r'/{group_id:\w+}/users/{user_id:\d+}', delete_user_from_group)
    app.router.add_get(prefix + r'/{group_id:\w+}/permissions', get_group_permissions_by_id)
