import aiohttp.web

from account.api.v1.group import routes as group
from account.api.v1.permission import routes as permission
from account.api.v1.token import routes as token
from account.api.v1.user import routes as user
from account.utils.urls import include_urls


def init_routes(app: aiohttp.web.Application, prefix):
    include_urls(app, prefix + '/group', group)
    include_urls(app, prefix + '/permission', permission)
    include_urls(app, prefix + '/token', token)
    include_urls(app, prefix + '/user', user)
