import aiohttp.web

from account.api.v1 import routes as v1
from account.utils.urls import include_urls


def init_routes(app: aiohttp.web.Application, prefix):
    include_urls(app, prefix + '/v1', v1)
