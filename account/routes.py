import aiohttp.web

from account.api import routes as api_urls
from account.utils.urls import include_urls


def init_routes(app: aiohttp.web.Application) -> None:
    include_urls(app, "/api", api_urls)
