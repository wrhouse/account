import asyncio
from datetime import datetime, timedelta

from account.db.models import Token


async def expire_token(settings, engine) -> None:
    token_expire_timedelta = timedelta(seconds=settings.access_token_expire_in)
    while True:
        update_query = (Token.__table__
                        .update()
                        .where(Token.updated_at < (datetime.now() - token_expire_timedelta))
                        .values(is_expired=True))

        async with engine.acquire() as connection:
            await connection.execute(update_query)

        await asyncio.sleep(settings.token_expiration_check_delay)
